let datetime_ids = [];

function getTables() {
    $.ajax({
        type: "POST",
        url: '/ratosql/',
        data: {
            'database': 'true',
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        },
        success: function (data) {
            let resp = JSON.parse(data);
            let database = resp['database'];
            let ind = 0;
            for (let key in database) {
                if (database.hasOwnProperty(key)) {
                    let table = $("<li class='table'></li>");
                    let table_name = $("<span class='table-name'></span>");
                    table_name[0].innerText = key;
                    let arrow =  $("<i class='arrow arrow-bottom'></i>");
                    arrow.attr("id","icon" + ind++);
                    arrow.click(toggleTableContents);
                    let columns = $("<ul class='columns'></ul>");
                    let attr_str = database[key];
                    attr_str = attr_str.replace('[', '');
                    attr_str = attr_str.replace(']', '');
                    let attr = attr_str.split(',');
                    for (let i = 0; i < attr.length; i = i + 2) {
                        attr[i] = attr[i] + ',' + attr[i + 1];
                    }
                    for (let i = 0; i < attr.length; i = i + 2) {
                        let column = $("<li class='column'></li>");
                        let column_name = $("<div class='column-name'></div>");
                        let column_type = $("<div class='column-type'></div>");
                        column.attr("id","table-columns-" + i);
                        let txt = getNameAndType(attr[i]);
                        txt = txt.split("*");
                        column_name[0].innerText = txt[0];
                        column_type[0].innerText = txt[1];
                        column.append(column_name);
                        column.append(column_type);
                        columns.append(column);
                    }
                    table.append(arrow);
                    table.append(table_name);
                    table.append(columns);
                    $(".tables").append(table);
                }
            }
        },
    }).fail(function () {
        console.log('request failed');
    });
}

$( document ).ready(function() {
    getTables();
    $(".operator-button").click(getSymbol);
    $("#executeButton").click(submitUserInput);
    $("#calculatorFrame").bind('keypress', checkKeyboardCombinations);
});

function parse_abstract_syntax_tree(text) {
    let text_sep = text.split('\n');
    let i;
    for (i=0; i<text_sep.length; i++) {
        let row = $('<div class="row-parse-tree"></div>')
        row[0].innerText=text_sep[i];
        $("#treeShow").append(row);
    }
}

function getNameAndType(data) {
    data = data.replace(/[\])}[{(]/g, '');
    let spl = data.split(',');
    let name = spl[0].trim();
    name = name.replace(/'/g, '');
    let type = spl[1].split(":")[1].trim();
    type = type.replace(/['>]/g, '');
    return name + '*' + type
}

function toggleTableContents() {
    if ($(this).siblings().hasClass("columns")) {
        $(this).siblings(".columns").slideToggle("fast", function () {
            if ($(this).siblings("i").hasClass("arrow-bottom")) {
                $(this).siblings("i").removeClass("arrow-bottom");
                $(this).siblings("i").addClass("arrow-top");
                $(this).css("border-left", "1px solid #305252");
                $(this).parent().css("border-bottom", "1px solid #305252");
            }
            else {
                $(this).siblings("i").addClass("arrow-bottom");
                $(this).siblings("i").removeClass("arrow-top");
                $(this).css("border-left", "none");
                $(this).parent().css("border-bottom", "none");
            }
        })
    }
}

function checkKeyboardCombinations(event) {
    if (event.shiftKey) {
        switch (event.which) {
            case 65:
                writeSymbol('σ');
                event.preventDefault();
                break;
            case 66:
                writeSymbol('Π');
                event.preventDefault();
                break;
            case 67:
                writeSymbol('⋈');
                event.preventDefault();
                break;
            case 68:
                writeSymbol('θ-join');
                event.preventDefault();
                break;
            case 69:
                writeSymbol('⨯');
                event.preventDefault();
                break;
            case 70:
                writeSymbol('∪');
                event.preventDefault();
                break;
            case 71:
                writeSymbol('∩');
                event.preventDefault();
                break;
            case 72:
                writeSymbol('-');
                event.preventDefault();
                break;
            case 73:
                writeSymbol('ρ');
                event.preventDefault();
                break;
            case 74:
                writeSymbol('γ');
                event.preventDefault();
                break;
            case 75:
                writeSymbol('sum');
                event.preventDefault();
                break;
            case 76:
                writeSymbol('count');
                event.preventDefault();
                break;
            case 77:
                writeSymbol('avg');
                event.preventDefault();
                break;
            case 78:
                writeSymbol('min');
                event.preventDefault();
                break;
            case 79:
                writeSymbol('max');
                event.preventDefault();
                break;
            default:
                break;
        }
    }
}

function getSymbol() {
    let s = this.children[0].children[0].innerText;
    writeSymbol(s);
}

function writeSymbol(symbol){
    let cursorPos = $("#calculatorFrame").prop('selectionStart');
    let value = $("#calculatorFrame").val();
    let textBefore = value.substring(0,  cursorPos);
    let textAfter  = value.substring(cursorPos, value.length);
    $("#calculatorFrame").val(textBefore + symbol + textAfter);
}

function createResultsTable(attr, content) {
  datetime_ids = [];
  let table = $('<table></table>');
  table.addClass('results-table');
  let rows = content.split('\n');
  let table_header = attr.split(',');
  let header_row = $('<tr id="header-row"></tr>');
  let iter;
  for (iter = 0; iter < table_header.length; iter++) {
      let column_name = table_header[iter].split(':')[0];
      let column_type = table_header[iter].split(':')[1];
      let header = $('<th></th>');
      header.addClass('results-header');
      column_name = column_name.replace(/;/g, ',');
      header[0].innerText = column_name;
      header_row.append(header);
      if(column_type === 'datetime') {
          datetime_ids.push(iter);
      }
  }
  table.append(header_row);
  let i, j;
  for (i = 0; i < rows.length - 1; i++) {
      let row = $('<tr></tr>');
      row.addClass('results-row');
      let columns = rows[i].split('* ');
      for (j = 0; j < columns.length; j++) {
          let cell = $('<td></td>');
          cell.addClass('results-data');
          for (iter = 0; iter < datetime_ids.length; iter++) {
              if (j === datetime_ids[iter]) {
                  columns[j] = columns[j].trim().split(' ')[0];
              }
          }
          if (columns[j].trim() === 'None') {
              cell[0].innerText = '-';
          }
          else {
              cell[0].innerText = columns[j].trim();
          }
          row.append(cell);
      }
      table.append(row);
  }
  $(".page-wrapper").append(table);
}

function submitUserInput(e) {
    e.preventDefault();
    $("#treeShow").hide();
    let table = $('.results-table');
    if (table.length !== 0) {
        table.remove();
    }
    let info = $("#calculatorFrame").val();
    info = info.trim();
    if (info.slice(-1)!==';') {
        info += ";"
    }
    $(".response-message").hide();
    if (info.length > 1) {
        $.ajax({
        type: "POST",
        url: '/ratosql/',
        data: {
            query: info,
            database: 'false',
            csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
        },
        success: function (data) {
            let resp = JSON.parse(data);
            if (resp['exception'] !== '') {
                //error at parsing
                $("#errorContainer").show();
                $("#errorContainer")[0].innerText = resp['exception'];
            }
            else if(resp['tuples'] !== null && resp['content'] === ''){
                //successful query but no record in the database
                let tup = '';
                if (resp['tuples'].trim() === 'no tuples returned') {
                    tup = 'There are no tuples which match your query.';
                }
                else {
                    tup = resp['tuples'];
                }
                let tree_text = $('<div id="showParseTree"></div>');
                let tree_show = $('<div id="treeShow"></div>');
                tree_text[0].innerText = 'Show generated abstract syntax tree';
                $("#noOfTuples")[0].innerText = tup;
                $("#noOfTuples").append(tree_text);
                $("#noOfTuples").show();
                $("#noOfTuples").css("display", "inline-block");
                $("#showParseTree").on( "click", function() {
                    if ($("#treeShow").is(':visible')) {
                        tree_show.hide();
                        tree_text[0].innerText = 'Show generated abstract syntax tree';
                    }
                    else {
                        parse_abstract_syntax_tree(resp["ast"]);
                        tree_show.show();
                        tree_text[0].innerText = 'Hide generated abstract syntax tree';
                        $(".right-container").append(tree_show);
                    }
                });
            }
            else if (resp['tuples'] !== null && resp['content'] !== null) {
                //successful query + results returned
                let tree_text = $('<div id="showParseTree"></div>');
                let tree_show = $('<div id="treeShow"></div>');
                tree_text[0].innerText = 'Show generated abstract syntax tree';
                $("#noOfTuples")[0].innerText = resp['tuples'];
                $("#noOfTuples").append(tree_text);
                $("#noOfTuples").show();
                $("#noOfTuples").css("display", "inline-block");
                $("#showParseTree").on( "click", function() {
                    if ($("#treeShow").is(':visible')) {
                        tree_show.hide();
                        tree_text[0].innerText = 'Show generated abstract syntax tree';
                    }
                    else {
                        $(".right-container").append(tree_show);
                        tree_show.show();
                        parse_abstract_syntax_tree(resp["ast"]);
                        tree_text[0].innerText = 'Hide Generated Parse tree';
                    }
                });
                createResultsTable(resp['attr'], resp['content']);
            }
        },
    }).fail(function() {
        console.log('request failed');
        $("#errorContainer").show();
        $("#errorContainer")[0].innerText = 'Operation failed/ We couldn\'t connect to the server';
    });
    }
    else {
        $("#errorContainer").show();
        $("#errorContainer")[0].innerText = 'Please enter a query before trying to execute!';
    }
}
