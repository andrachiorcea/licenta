let ra_cont = '';
let sql_cont = '';

$( document ).ready(function() {
    $.ajax({
      type: "POST",
      url: '/about/',
      data: {
          'req_files': 'true',
          csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
      },
      success: function (data) {
          let resp = JSON.parse(data);
          ra_cont = resp['ra'].split('\n');
          sql_cont = resp['sql'].split('\n');
          create_about_info_querys();
          },
      }).fail(function() {
            console.log('request failed');
            $("#errorContainer")[0].innerText = 'We couldn\'t connect to the server';
         });
});


function create_about_info_querys() {
  let i;
  for(i=0; i< sql_cont.length; i++) {
      let flip_card = $('<div></div>');
      let card = $('<div></div>');
      let card_sql = $('<div></div>');
      let card_ra = $('<div></div>');
      card_ra[0].innerText = sql_cont[i];
      card_sql[0].innerText = ra_cont[i];
      flip_card.addClass("flip-card");
      card.addClass("flip-card-inner");
      card_ra.addClass("flip-card-front");
      card_sql.addClass("flip-card-back");
      card.append(card_ra);
      card.append(card_sql);
      flip_card.append(card);
      $(".right-wrapper").append(flip_card);
  }
}
