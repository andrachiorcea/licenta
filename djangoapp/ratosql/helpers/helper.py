import re
import os

symbols = ["σ", "Π", "⋈", "θ-join", "⟕", "⟖", "⟗","⨯", "∪", "∩", "minus", "ρ", "γ", "sum", "count", "avg", "max", "min"]
cli_symbols = ["\select_", "\project_", "\join", "\join_", "\left_outer_join_", "\\right_outer_join_","\\full_outer_join_", "\cross", "\\union", "\intersect", "\diff", "\\rename_",
               "\\aggr_", "sum", "count", "avg", "max", "min", "\\\'"]
cli_symbols_err = ["\select", "\project", "\join","\join", "\left_outer_join",  "\\right_outer_join_","\\full_outer_join_", "\cross", "\\union", "\intersect", "\diff", "\\rename",
               "\\aggr", "sum", "count", "avg", "max", "min", "\\\'"]
cli_symbols_all = ["\select_", "\project_", "\join", "\join_", "\left_outer_join_",  "\\right_outer_join_","\\full_outer_join_", "\cross", "\\union", "\intersect", "\diff", "\\rename_",
               "\\aggr_", "\sum", "\count", "\\avg", "\max", "\min",  "{",  "}", "\'", "\list", "\clear", "\save", "\source",
               "\sqlexec"]
cli_symbols_all_err = ["\list", "\clear", "\save", "\source", "\sqlexec"]
ra_file_about = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../static/about/ra.txt')
sql_file_about = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../static/about/sql.txt')


def replace_spaces_from_rename(fe_query, direction):
    if direction == 0:
        if (re.match('ρ{[A-Za-z0-9!@#$%^&*()_\-.,;\' ]*}', fe_query)):
            match = re.findall('ρ{[A-Za-z0-9!@#$%^&*()_\-.,;\' ]*}', fe_query)[0]
            match = re.sub(" ", "space", match)
            fe_query = re.sub('ρ{[A-Za-z0-9!@#$%^&*()_\-.,;\' ]*}', match, fe_query)
    elif direction == 1:
        fe_query = re.sub("space", " ", fe_query)
    return fe_query


def user_to_cli(fe_query):
    for i in range(0, len(symbols)):
        fe_query = fe_query.replace(symbols[i], cli_symbols[i])
    print(fe_query)
    return fe_query


def cli_to_user(be_query):
    for i in range(0, len(symbols)):
        be_query = be_query.replace(cli_symbols_err[i], symbols[i])
    return be_query


def get_attr_without_rename(fe_query):
    match = ''
    if re.search('γ{[A-Za-z0-9!@#$%^&*:().,_/;\-\'| ]*}', fe_query):
        match = re.findall('γ{[A-Za-z0-9!@#$%^&*:().,_/;\-\'| ]*}', fe_query)[0]
        if '(' in match:
            print(True)
        match = re.sub("γ", "", match)
        match = re.sub("{", "", match)
        match = re.sub("}", "", match)
        match = re.sub(" ", "", match)
        attr_hol = match.split(":")
        attr = attr_hol[0]
        if len(attr_hol) > 1:
            grp = attr_hol[1]
            match = attr + ',' + grp
        else:
            match = attr
    elif re.search('Π{[A-Za-z0-9!@#$%^&*().,_/;\-\'| ]*}', fe_query):
        match = re.findall('Π{[A-Za-z0-9!@#$%^&*().,_/;\-\'| ]*}', fe_query)[0]
        if '(' in match:
            total_number_of_open_brackets = 0
            number_of_open_brackets = 0
            number_of_closing_brackets = 0
            for l in match:
                if l == '(':
                    number_of_open_brackets += 1
                    if total_number_of_open_brackets < number_of_open_brackets:
                        total_number_of_open_brackets = number_of_open_brackets
                elif l == ')':
                    number_of_closing_brackets += 1
                    number_of_open_brackets -= 1
                elif l == ',':
                    if number_of_closing_brackets < total_number_of_open_brackets:
                        match = re.sub(",", ";", match)
        match = re.sub("Π", "", match)
        match = re.sub("{", "", match)
        match = re.sub("}", "", match)
        match = re.sub(" ", "", match)
    return match


def modify_parsing_error_message(error):
    arguments = re.findall('\'\\\[A-Za-z]*\'', error)
    to_delete = []
    to_keep = []
    sep = ' '
    for el in arguments:
        new_el = ''
        for letter in el:
            if letter != '\'':
                new_el += letter
        el = new_el
        if el in cli_symbols_all_err:
            to_delete.append(el)
    words = error.split(' ')
    for word in words:
        if word == '\'_{\'':
            to_keep.append('{')
        elif word == '\'_}\'':
            to_keep.append('}')
        elif word == '_':
            to_keep.append('')
        elif word not in to_delete:
            to_keep.append(word)
    cursor_error = words[1].split(':')[0]
    to_keep[1] = cursor_error
    to_ret = sep.join(to_keep)
    to_ret = cli_to_user(to_ret)
    return to_ret
