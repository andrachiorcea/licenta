from django.shortcuts import render
from .helpers.helper import user_to_cli
import os, io
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
from django.http import HttpResponse
from .parser.radb.ra import config, callable_main
from .helpers.helper import modify_parsing_error_message, replace_spaces_from_rename, get_attr_without_rename
import json

context = None
ra_file_about = os.path.join(os.path.dirname(os.path.realpath(__file__)), './static/about/ra.txt')
sql_file_about = os.path.join(os.path.dirname(os.path.realpath(__file__)), './static/about/sql.txt')


def ratosql_page(request):
    global context
    if request.method == "GET":
        context = config()
        return render(request, 'ratosql.html', {'data': ''})
    elif request.method == "POST":
        if context is None:
            context = config()
        if request.POST['database'] == 'false':
            query = request.POST['query']
            query_process = replace_spaces_from_rename(query, 0)
            query_process = user_to_cli(query_process)
            (attr, res, tuples, exception, ast) = callable_main(context, query_process)
            attr = replace_spaces_from_rename(attr, 1)
            has_unnamed = False
            attrs = attr.split(',')
            for a in attrs:
                a = a.split(':')[0].strip()
                if a == '_':
                    has_unnamed = True
            if has_unnamed == True:
                attr = get_attr_without_rename(query)
            if exception == '':
                ex = ''
            else:
                ex = modify_parsing_error_message(exception.args[0])
            response = {
                "content": res,
                "tuples": tuples,
                "exception": ex,
                "attr": attr,
                "ast": ast
            }
            to_ret = json.dumps(response)
            return HttpResponse(to_ret)
        elif request.POST['database'] == 'true':
            tables = context.db.list()
            database = {}
            for table in tables:
                attrs = context.db.describe(table)
                database[table] = str(attrs)
            response = {
                "database": database
            }
            to_ret = json.dumps(response)
            return HttpResponse(to_ret)


@csrf_exempt
def about(request):
    response = {}
    if request.method == "GET":
        return render(request, 'about.html', {'data': ''})
    elif request.method == "POST":
        if request.POST['req_files'] == 'true':
            ra_file = io.open(ra_file_about, mode='r', encoding="utf-8").read()
            sql_file = io.open(sql_file_about, mode='r', encoding="utf-8").read()
            response = {
                'ra': ra_file,
                'sql': sql_file
            }
        to_ret = json.dumps(response)
        return HttpResponse(to_ret)


def handler404(request):
    return render(request, 'about.html', status=404)


def handler500(request):
    return render(request, '500.html', status=500)

