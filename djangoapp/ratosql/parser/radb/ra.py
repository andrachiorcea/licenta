import os
import sys
import configparser
import argparse

try:
    import readline
except ImportError:
    _readline_available = False
else:
    _readline_available = True

from .db import DB
from .parse import ParsingError, one_statement_from_string
from .typesys import ValTypeChecker, TypeSysError
from .views import ViewCollection
from .ast import Context, ValidationError, ExecutionError

import logging
logger = logging.getLogger('ra')

def config():
    # read system defaults:
    sys_configfile = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'sys.ini')
    sys_config = configparser.ConfigParser()
    if sys_config.read(sys_configfile) == []:
        print('ERROR: required system configuration file {} not found'.format(sys_configfile))
        sys.exit(1)
    defaults = dict(sys_config.items(configparser.DEFAULTSECT))
    # parse input arguments:
    parser = argparse.ArgumentParser()
    parser.add_argument('source', type=str, nargs='?', default=configparser.DEFAULTSECT,
                        help='data source, which can be the name of a configuration section'
                        ' (default: {}), or otherwise the name of the database to connect to'
                        ' (overriding the configuration default)'.format(configparser.DEFAULTSECT))
    args = parser.parse_args([])


    # set up output replication + logging:
    logging.getLogger('ra').setLevel(logging.INFO)
    logger_handler = logging.StreamHandler(sys.stdout)
    logger_handler.setFormatter(logging.Formatter('%(levelname)s: %(message)s'))
    logger.addHandler(logger_handler)

    # read user configuration file (starting with system defaults):
    config = configparser.ConfigParser(defaults)

    # finalize configuration settings, using configuration file and command-line arguments:
    if args.source == configparser.DEFAULTSECT or config.has_section(args.source):
        configured = dict(config.items(args.source))
    else: # args.source is not a section in the config file; treat it as a database name:
        configured = dict(config.items(configparser.DEFAULTSECT))
        configured['db.database'] = args.source

    # connect to database:
    if 'db.database' not in configured:
        logger.warning('no database specified')
    try:
        db = DB(configured)
    except Exception as e:
        logger.error('failed to connect to database: {}'.format(e))
        sys.exit(1)

    # initialize type system:
    try:
        check = ValTypeChecker(configured['default_functions'], configured.get('functions', None))
    except TypeSysError as e:
        logger.error(e)
        sys.exit(1)

    # construct context (starting with empty view collection):
    context = Context(db, check, ViewCollection())
    return context


def callable_main(context, query):
    tuples = ''
    exception = ''
    result = ''
    attr = ''
    ast_to_ret = ''
    try:
        ast = one_statement_from_string(query)
        logger.info('statement parsed:')
        logger.info(str(ast))
        ast.validate(context)
        logger.info('statement validated:')
        for line in ast.info():
            logger.info(line)
            ast_to_ret += line + '\n'
        (attr, result, tuples) = ast.execute(context)
    except (ParsingError, ValidationError, ExecutionError) as e:
        logger.error(e)
        exception = e
    return attr, result, tuples, exception, ast_to_ret

