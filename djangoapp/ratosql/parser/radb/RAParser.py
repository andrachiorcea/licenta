# Generated from C:/Users/achio/Documents/AnulIII/Semestrul2/licenta/djangoapp/ratosql/parser/radb\RAParser.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\38")
        buf.write("\u00dc\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3")
        buf.write("\2\5\2\35\n\2\3\2\3\2\3\2\5\2\"\n\2\3\2\3\2\3\2\5\2\'")
        buf.write("\n\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3")
        buf.write("\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\7\2")
        buf.write("B\n\2\f\2\16\2E\13\2\3\3\3\3\3\3\5\3J\n\3\3\4\3\4\3\4")
        buf.write("\5\4O\n\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5")
        buf.write("\3\5\5\5]\n\5\3\5\5\5`\n\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5")
        buf.write("\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5u")
        buf.write("\n\5\3\5\3\5\3\5\5\5z\n\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5")
        buf.write("\u0082\n\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5\u008b\n\5\3")
        buf.write("\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5\u0094\n\5\3\5\3\5\3\5\3")
        buf.write("\5\3\5\3\5\3\5\5\5\u009d\n\5\3\5\3\5\3\5\3\5\3\5\3\5\3")
        buf.write("\5\3\5\3\5\3\5\3\5\3\5\3\5\7\5\u00ac\n\5\f\5\16\5\u00af")
        buf.write("\13\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\5\7\u00b8\n\7\3\7\3")
        buf.write("\7\5\7\u00bc\n\7\3\7\3\7\5\7\u00c0\n\7\3\7\3\7\5\7\u00c4")
        buf.write("\n\7\3\7\3\7\3\7\3\7\3\7\5\7\u00cb\n\7\3\b\3\b\3\b\5\b")
        buf.write("\u00d0\n\b\3\b\3\b\3\t\7\t\u00d5\n\t\f\t\16\t\u00d8\13")
        buf.write("\t\3\t\3\t\3\t\2\4\2\b\n\2\4\6\b\n\f\16\20\2\6\3\2!\"")
        buf.write("\3\2#$\3\2,\61\4\2\22\22!!\2\u0105\2&\3\2\2\2\4F\3\2\2")
        buf.write("\2\6K\3\2\2\2\by\3\2\2\2\n\u00b0\3\2\2\2\f\u00ca\3\2\2")
        buf.write("\2\16\u00cf\3\2\2\2\20\u00d6\3\2\2\2\22\23\b\2\1\2\23")
        buf.write("\'\7\20\2\2\24\'\7\21\2\2\25\26\7&\2\2\26\27\5\2\2\2\27")
        buf.write("\30\7\'\2\2\30\'\3\2\2\2\31\32\7\22\2\2\32\34\7&\2\2\33")
        buf.write("\35\5\4\3\2\34\33\3\2\2\2\34\35\3\2\2\2\35\36\3\2\2\2")
        buf.write("\36\'\7\'\2\2\37 \7\22\2\2 \"\7\37\2\2!\37\3\2\2\2!\"")
        buf.write("\3\2\2\2\"#\3\2\2\2#\'\7\22\2\2$%\7\17\2\2%\'\5\2\2\5")
        buf.write("&\22\3\2\2\2&\24\3\2\2\2&\25\3\2\2\2&\31\3\2\2\2&!\3\2")
        buf.write("\2\2&$\3\2\2\2\'C\3\2\2\2()\f\f\2\2)*\t\2\2\2*B\5\2\2")
        buf.write("\r+,\f\13\2\2,-\t\3\2\2-B\5\2\2\f./\f\n\2\2/\60\7%\2\2")
        buf.write("\60B\5\2\2\13\61\62\f\t\2\2\62\63\t\4\2\2\63B\5\2\2\n")
        buf.write("\64\65\f\b\2\2\65\66\7\f\2\2\66B\5\2\2\t\678\f\4\2\28")
        buf.write("9\7\r\2\29B\5\2\2\5:;\f\3\2\2;<\7\16\2\2<B\5\2\2\4=>\f")
        buf.write("\7\2\2>B\7\b\2\2?@\f\6\2\2@B\7\t\2\2A(\3\2\2\2A+\3\2\2")
        buf.write("\2A.\3\2\2\2A\61\3\2\2\2A\64\3\2\2\2A\67\3\2\2\2A:\3\2")
        buf.write("\2\2A=\3\2\2\2A?\3\2\2\2BE\3\2\2\2CA\3\2\2\2CD\3\2\2\2")
        buf.write("D\3\3\2\2\2EC\3\2\2\2FI\5\2\2\2GH\7 \2\2HJ\5\4\3\2IG\3")
        buf.write("\2\2\2IJ\3\2\2\2J\5\3\2\2\2KN\7\22\2\2LM\7 \2\2MO\5\6")
        buf.write("\4\2NL\3\2\2\2NO\3\2\2\2O\7\3\2\2\2PQ\b\5\1\2QR\7&\2\2")
        buf.write("RS\5\b\5\2ST\7\'\2\2Tz\3\2\2\2Uz\7\22\2\2VW\7\23\2\2W")
        buf.write("_\7(\2\2XY\7\22\2\2Y\\\7+\2\2Z]\7!\2\2[]\5\6\4\2\\Z\3")
        buf.write("\2\2\2\\[\3\2\2\2]`\3\2\2\2^`\5\6\4\2_X\3\2\2\2_^\3\2")
        buf.write("\2\2`a\3\2\2\2ab\7)\2\2bz\5\b\5\16cd\7\24\2\2de\7(\2\2")
        buf.write("ef\5\4\3\2fg\7)\2\2gh\5\b\5\rhz\3\2\2\2ij\7\25\2\2jk\7")
        buf.write("(\2\2kl\5\2\2\2lm\7)\2\2mn\5\b\5\fnz\3\2\2\2op\7\36\2")
        buf.write("\2pq\7(\2\2qt\5\4\3\2rs\7+\2\2su\5\4\3\2tr\3\2\2\2tu\3")
        buf.write("\2\2\2uv\3\2\2\2vw\7)\2\2wx\5\b\5\3xz\3\2\2\2yP\3\2\2")
        buf.write("\2yU\3\2\2\2yV\3\2\2\2yc\3\2\2\2yi\3\2\2\2yo\3\2\2\2z")
        buf.write("\u00ad\3\2\2\2{|\f\13\2\2|\u0081\7\26\2\2}~\7(\2\2~\177")
        buf.write("\5\2\2\2\177\u0080\7)\2\2\u0080\u0082\3\2\2\2\u0081}\3")
        buf.write("\2\2\2\u0081\u0082\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u00ac")
        buf.write("\5\b\5\f\u0084\u0085\f\n\2\2\u0085\u008a\7\27\2\2\u0086")
        buf.write("\u0087\7(\2\2\u0087\u0088\5\2\2\2\u0088\u0089\7)\2\2\u0089")
        buf.write("\u008b\3\2\2\2\u008a\u0086\3\2\2\2\u008a\u008b\3\2\2\2")
        buf.write("\u008b\u008c\3\2\2\2\u008c\u00ac\5\b\5\13\u008d\u008e")
        buf.write("\f\t\2\2\u008e\u0093\7\30\2\2\u008f\u0090\7(\2\2\u0090")
        buf.write("\u0091\5\2\2\2\u0091\u0092\7)\2\2\u0092\u0094\3\2\2\2")
        buf.write("\u0093\u008f\3\2\2\2\u0093\u0094\3\2\2\2\u0094\u0095\3")
        buf.write("\2\2\2\u0095\u00ac\5\b\5\n\u0096\u0097\f\b\2\2\u0097\u009c")
        buf.write("\7\31\2\2\u0098\u0099\7(\2\2\u0099\u009a\5\2\2\2\u009a")
        buf.write("\u009b\7)\2\2\u009b\u009d\3\2\2\2\u009c\u0098\3\2\2\2")
        buf.write("\u009c\u009d\3\2\2\2\u009d\u009e\3\2\2\2\u009e\u00ac\5")
        buf.write("\b\5\t\u009f\u00a0\f\7\2\2\u00a0\u00a1\7\32\2\2\u00a1")
        buf.write("\u00ac\5\b\5\b\u00a2\u00a3\f\6\2\2\u00a3\u00a4\7\33\2")
        buf.write("\2\u00a4\u00ac\5\b\5\7\u00a5\u00a6\f\5\2\2\u00a6\u00a7")
        buf.write("\7\34\2\2\u00a7\u00ac\5\b\5\6\u00a8\u00a9\f\4\2\2\u00a9")
        buf.write("\u00aa\7\35\2\2\u00aa\u00ac\5\b\5\5\u00ab{\3\2\2\2\u00ab")
        buf.write("\u0084\3\2\2\2\u00ab\u008d\3\2\2\2\u00ab\u0096\3\2\2\2")
        buf.write("\u00ab\u009f\3\2\2\2\u00ab\u00a2\3\2\2\2\u00ab\u00a5\3")
        buf.write("\2\2\2\u00ab\u00a8\3\2\2\2\u00ac\u00af\3\2\2\2\u00ad\u00ab")
        buf.write("\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae\t\3\2\2\2\u00af\u00ad")
        buf.write("\3\2\2\2\u00b0\u00b1\7\22\2\2\u00b1\u00b2\7*\2\2\u00b2")
        buf.write("\u00b3\5\b\5\2\u00b3\13\3\2\2\2\u00b4\u00cb\7\62\2\2\u00b5")
        buf.write("\u00bb\7\63\2\2\u00b6\u00b8\7\7\2\2\u00b7\u00b6\3\2\2")
        buf.write("\2\u00b7\u00b8\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9\u00bc")
        buf.write("\7\22\2\2\u00ba\u00bc\7!\2\2\u00bb\u00b7\3\2\2\2\u00bb")
        buf.write("\u00ba\3\2\2\2\u00bc\u00cb\3\2\2\2\u00bd\u00bf\7\64\2")
        buf.write("\2\u00be\u00c0\7\7\2\2\u00bf\u00be\3\2\2\2\u00bf\u00c0")
        buf.write("\3\2\2\2\u00c0\u00c1\3\2\2\2\u00c1\u00c3\t\5\2\2\u00c2")
        buf.write("\u00c4\7\20\2\2\u00c3\u00c2\3\2\2\2\u00c3\u00c4\3\2\2")
        buf.write("\2\u00c4\u00cb\3\2\2\2\u00c5\u00c6\7\65\2\2\u00c6\u00cb")
        buf.write("\7\20\2\2\u00c7\u00cb\7\66\2\2\u00c8\u00c9\7\67\2\2\u00c9")
        buf.write("\u00cb\78\2\2\u00ca\u00b4\3\2\2\2\u00ca\u00b5\3\2\2\2")
        buf.write("\u00ca\u00bd\3\2\2\2\u00ca\u00c5\3\2\2\2\u00ca\u00c7\3")
        buf.write("\2\2\2\u00ca\u00c8\3\2\2\2\u00cb\r\3\2\2\2\u00cc\u00d0")
        buf.write("\5\b\5\2\u00cd\u00d0\5\n\6\2\u00ce\u00d0\5\f\7\2\u00cf")
        buf.write("\u00cc\3\2\2\2\u00cf\u00cd\3\2\2\2\u00cf\u00ce\3\2\2\2")
        buf.write("\u00d0\u00d1\3\2\2\2\u00d1\u00d2\7\6\2\2\u00d2\17\3\2")
        buf.write("\2\2\u00d3\u00d5\5\16\b\2\u00d4\u00d3\3\2\2\2\u00d5\u00d8")
        buf.write("\3\2\2\2\u00d6\u00d4\3\2\2\2\u00d6\u00d7\3\2\2\2\u00d7")
        buf.write("\u00d9\3\2\2\2\u00d8\u00d6\3\2\2\2\u00d9\u00da\7\2\2\3")
        buf.write("\u00da\21\3\2\2\2\32\34!&ACIN\\_ty\u0081\u008a\u0093\u009c")
        buf.write("\u00ab\u00ad\u00b7\u00bb\u00bf\u00c3\u00ca\u00cf\u00d6")
        return buf.getvalue()


class RAParser ( Parser ):

    grammarFileName = "RAParser.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "';'", "'!'", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "'\\rename'", "'\\project'", "'\\select'", "'\\join'", 
                     "'\\left_outer_join'", "'\\right_outer_join'", "'\\full_outer_join'", 
                     "'\\cross'", "'\\union'", "'\\diff'", "'\\intersect'", 
                     "'\\aggr'", "'.'", "','", "'*'", "'/'", "'+'", "'-'", 
                     "'||'", "'('", "')'", "'_{'", "'}'", "':-'", "':'", 
                     "'<='", "'<>'", "'>='", "'<'", "'='", "'>'", "'\\list'", 
                     "'\\clear'", "'\\save'", "'\\source'", "'\\quit'", 
                     "'\\sqlexec'" ]

    symbolicNames = [ "<INVALID>", "WS", "COMMENT", "LINE_COMMENT", "TERMINATOR", 
                      "FORCE", "IS_NULL", "IS_NOT_NULL", "IS", "NULL", "LIKE", 
                      "AND", "OR", "NOT", "STRING", "NUMBER", "ID", "RENAME", 
                      "PROJECT", "SELECT", "JOIN", "LEFT_OUTER_JOIN", "RIGHT_OUTER_JOIN", 
                      "FULL_OUTER_JOIN", "CROSS", "UNION", "DIFF", "INTERSECT", 
                      "AGGR", "DOT", "COMMA", "STAR", "SLASH", "PLUS", "MINUS", 
                      "CONCAT", "PAREN_L", "PAREN_R", "ARG_L", "ARG_R", 
                      "GETS", "COLON", "LE", "NE", "GE", "LT", "EQ", "GT", 
                      "LIST", "CLEAR", "SAVE", "SOURCE", "QUIT", "SQLEXEC", 
                      "SQLEXEC_TEXT" ]

    RULE_valExpr = 0
    RULE_listOfValExprs = 1
    RULE_listOfIDs = 2
    RULE_relExpr = 3
    RULE_definition = 4
    RULE_command = 5
    RULE_statement = 6
    RULE_program = 7

    ruleNames =  [ "valExpr", "listOfValExprs", "listOfIDs", "relExpr", 
                   "definition", "command", "statement", "program" ]

    EOF = Token.EOF
    WS=1
    COMMENT=2
    LINE_COMMENT=3
    TERMINATOR=4
    FORCE=5
    IS_NULL=6
    IS_NOT_NULL=7
    IS=8
    NULL=9
    LIKE=10
    AND=11
    OR=12
    NOT=13
    STRING=14
    NUMBER=15
    ID=16
    RENAME=17
    PROJECT=18
    SELECT=19
    JOIN=20
    LEFT_OUTER_JOIN=21
    RIGHT_OUTER_JOIN=22
    FULL_OUTER_JOIN=23
    CROSS=24
    UNION=25
    DIFF=26
    INTERSECT=27
    AGGR=28
    DOT=29
    COMMA=30
    STAR=31
    SLASH=32
    PLUS=33
    MINUS=34
    CONCAT=35
    PAREN_L=36
    PAREN_R=37
    ARG_L=38
    ARG_R=39
    GETS=40
    COLON=41
    LE=42
    NE=43
    GE=44
    LT=45
    EQ=46
    GT=47
    LIST=48
    CLEAR=49
    SAVE=50
    SOURCE=51
    QUIT=52
    SQLEXEC=53
    SQLEXEC_TEXT=54

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ValExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return RAParser.RULE_valExpr

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class NumberLiteralValExprContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def NUMBER(self):
            return self.getToken(RAParser.NUMBER, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNumberLiteralValExpr" ):
                listener.enterNumberLiteralValExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNumberLiteralValExpr" ):
                listener.exitNumberLiteralValExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNumberLiteralValExpr" ):
                return visitor.visitNumberLiteralValExpr(self)
            else:
                return visitor.visitChildren(self)


    class IsNotNullExprContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def valExpr(self):
            return self.getTypedRuleContext(RAParser.ValExprContext,0)

        def IS_NOT_NULL(self):
            return self.getToken(RAParser.IS_NOT_NULL, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIsNotNullExpr" ):
                listener.enterIsNotNullExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIsNotNullExpr" ):
                listener.exitIsNotNullExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIsNotNullExpr" ):
                return visitor.visitIsNotNullExpr(self)
            else:
                return visitor.visitChildren(self)


    class FuncExprContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(RAParser.ID, 0)
        def PAREN_L(self):
            return self.getToken(RAParser.PAREN_L, 0)
        def PAREN_R(self):
            return self.getToken(RAParser.PAREN_R, 0)
        def listOfValExprs(self):
            return self.getTypedRuleContext(RAParser.ListOfValExprsContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFuncExpr" ):
                listener.enterFuncExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFuncExpr" ):
                listener.exitFuncExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuncExpr" ):
                return visitor.visitFuncExpr(self)
            else:
                return visitor.visitChildren(self)


    class AttrRefContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(RAParser.ID)
            else:
                return self.getToken(RAParser.ID, i)
        def DOT(self):
            return self.getToken(RAParser.DOT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAttrRef" ):
                listener.enterAttrRef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAttrRef" ):
                listener.exitAttrRef(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAttrRef" ):
                return visitor.visitAttrRef(self)
            else:
                return visitor.visitChildren(self)


    class PlusMinusExprContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def valExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.ValExprContext)
            else:
                return self.getTypedRuleContext(RAParser.ValExprContext,i)

        def PLUS(self):
            return self.getToken(RAParser.PLUS, 0)
        def MINUS(self):
            return self.getToken(RAParser.MINUS, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPlusMinusExpr" ):
                listener.enterPlusMinusExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPlusMinusExpr" ):
                listener.exitPlusMinusExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPlusMinusExpr" ):
                return visitor.visitPlusMinusExpr(self)
            else:
                return visitor.visitChildren(self)


    class OrExprContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def valExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.ValExprContext)
            else:
                return self.getTypedRuleContext(RAParser.ValExprContext,i)

        def OR(self):
            return self.getToken(RAParser.OR, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterOrExpr" ):
                listener.enterOrExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitOrExpr" ):
                listener.exitOrExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOrExpr" ):
                return visitor.visitOrExpr(self)
            else:
                return visitor.visitChildren(self)


    class ValExprParenthesizedContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def PAREN_L(self):
            return self.getToken(RAParser.PAREN_L, 0)
        def valExpr(self):
            return self.getTypedRuleContext(RAParser.ValExprContext,0)

        def PAREN_R(self):
            return self.getToken(RAParser.PAREN_R, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterValExprParenthesized" ):
                listener.enterValExprParenthesized(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitValExprParenthesized" ):
                listener.exitValExprParenthesized(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitValExprParenthesized" ):
                return visitor.visitValExprParenthesized(self)
            else:
                return visitor.visitChildren(self)


    class ConcatExprContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def valExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.ValExprContext)
            else:
                return self.getTypedRuleContext(RAParser.ValExprContext,i)

        def CONCAT(self):
            return self.getToken(RAParser.CONCAT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConcatExpr" ):
                listener.enterConcatExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConcatExpr" ):
                listener.exitConcatExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitConcatExpr" ):
                return visitor.visitConcatExpr(self)
            else:
                return visitor.visitChildren(self)


    class NotExprContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def NOT(self):
            return self.getToken(RAParser.NOT, 0)
        def valExpr(self):
            return self.getTypedRuleContext(RAParser.ValExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterNotExpr" ):
                listener.enterNotExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitNotExpr" ):
                listener.exitNotExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNotExpr" ):
                return visitor.visitNotExpr(self)
            else:
                return visitor.visitChildren(self)


    class IsNullExprContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def valExpr(self):
            return self.getTypedRuleContext(RAParser.ValExprContext,0)

        def IS_NULL(self):
            return self.getToken(RAParser.IS_NULL, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIsNullExpr" ):
                listener.enterIsNullExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIsNullExpr" ):
                listener.exitIsNullExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIsNullExpr" ):
                return visitor.visitIsNullExpr(self)
            else:
                return visitor.visitChildren(self)


    class StringLiteralValExprContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def STRING(self):
            return self.getToken(RAParser.STRING, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStringLiteralValExpr" ):
                listener.enterStringLiteralValExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStringLiteralValExpr" ):
                listener.exitStringLiteralValExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStringLiteralValExpr" ):
                return visitor.visitStringLiteralValExpr(self)
            else:
                return visitor.visitChildren(self)


    class LikeExprContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def valExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.ValExprContext)
            else:
                return self.getTypedRuleContext(RAParser.ValExprContext,i)

        def LIKE(self):
            return self.getToken(RAParser.LIKE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLikeExpr" ):
                listener.enterLikeExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLikeExpr" ):
                listener.exitLikeExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLikeExpr" ):
                return visitor.visitLikeExpr(self)
            else:
                return visitor.visitChildren(self)


    class MultDivExprContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def valExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.ValExprContext)
            else:
                return self.getTypedRuleContext(RAParser.ValExprContext,i)

        def STAR(self):
            return self.getToken(RAParser.STAR, 0)
        def SLASH(self):
            return self.getToken(RAParser.SLASH, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMultDivExpr" ):
                listener.enterMultDivExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMultDivExpr" ):
                listener.exitMultDivExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMultDivExpr" ):
                return visitor.visitMultDivExpr(self)
            else:
                return visitor.visitChildren(self)


    class CompareExprContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def valExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.ValExprContext)
            else:
                return self.getTypedRuleContext(RAParser.ValExprContext,i)

        def LT(self):
            return self.getToken(RAParser.LT, 0)
        def LE(self):
            return self.getToken(RAParser.LE, 0)
        def EQ(self):
            return self.getToken(RAParser.EQ, 0)
        def NE(self):
            return self.getToken(RAParser.NE, 0)
        def GE(self):
            return self.getToken(RAParser.GE, 0)
        def GT(self):
            return self.getToken(RAParser.GT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCompareExpr" ):
                listener.enterCompareExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCompareExpr" ):
                listener.exitCompareExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCompareExpr" ):
                return visitor.visitCompareExpr(self)
            else:
                return visitor.visitChildren(self)


    class AndExprContext(ValExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.ValExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def valExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.ValExprContext)
            else:
                return self.getTypedRuleContext(RAParser.ValExprContext,i)

        def AND(self):
            return self.getToken(RAParser.AND, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAndExpr" ):
                listener.enterAndExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAndExpr" ):
                listener.exitAndExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAndExpr" ):
                return visitor.visitAndExpr(self)
            else:
                return visitor.visitChildren(self)



    def valExpr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = RAParser.ValExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 0
        self.enterRecursionRule(localctx, 0, self.RULE_valExpr, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 36
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                localctx = RAParser.StringLiteralValExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 17
                self.match(RAParser.STRING)
                pass

            elif la_ == 2:
                localctx = RAParser.NumberLiteralValExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 18
                self.match(RAParser.NUMBER)
                pass

            elif la_ == 3:
                localctx = RAParser.ValExprParenthesizedContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 19
                self.match(RAParser.PAREN_L)
                self.state = 20
                self.valExpr(0)
                self.state = 21
                self.match(RAParser.PAREN_R)
                pass

            elif la_ == 4:
                localctx = RAParser.FuncExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 23
                self.match(RAParser.ID)
                self.state = 24
                self.match(RAParser.PAREN_L)
                self.state = 26
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RAParser.NOT) | (1 << RAParser.STRING) | (1 << RAParser.NUMBER) | (1 << RAParser.ID) | (1 << RAParser.PAREN_L))) != 0):
                    self.state = 25
                    self.listOfValExprs()


                self.state = 28
                self.match(RAParser.PAREN_R)
                pass

            elif la_ == 5:
                localctx = RAParser.AttrRefContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 31
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
                if la_ == 1:
                    self.state = 29
                    self.match(RAParser.ID)
                    self.state = 30
                    self.match(RAParser.DOT)


                self.state = 33
                self.match(RAParser.ID)
                pass

            elif la_ == 6:
                localctx = RAParser.NotExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 34
                self.match(RAParser.NOT)
                self.state = 35
                self.valExpr(3)
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 65
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,4,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 63
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
                    if la_ == 1:
                        localctx = RAParser.MultDivExprContext(self, RAParser.ValExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_valExpr)
                        self.state = 38
                        if not self.precpred(self._ctx, 10):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 10)")
                        self.state = 39
                        _la = self._input.LA(1)
                        if not(_la==RAParser.STAR or _la==RAParser.SLASH):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 40
                        self.valExpr(11)
                        pass

                    elif la_ == 2:
                        localctx = RAParser.PlusMinusExprContext(self, RAParser.ValExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_valExpr)
                        self.state = 41
                        if not self.precpred(self._ctx, 9):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 9)")
                        self.state = 42
                        _la = self._input.LA(1)
                        if not(_la==RAParser.PLUS or _la==RAParser.MINUS):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 43
                        self.valExpr(10)
                        pass

                    elif la_ == 3:
                        localctx = RAParser.ConcatExprContext(self, RAParser.ValExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_valExpr)
                        self.state = 44
                        if not self.precpred(self._ctx, 8):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 8)")
                        self.state = 45
                        self.match(RAParser.CONCAT)
                        self.state = 46
                        self.valExpr(9)
                        pass

                    elif la_ == 4:
                        localctx = RAParser.CompareExprContext(self, RAParser.ValExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_valExpr)
                        self.state = 47
                        if not self.precpred(self._ctx, 7):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 7)")
                        self.state = 48
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RAParser.LE) | (1 << RAParser.NE) | (1 << RAParser.GE) | (1 << RAParser.LT) | (1 << RAParser.EQ) | (1 << RAParser.GT))) != 0)):
                            self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 49
                        self.valExpr(8)
                        pass

                    elif la_ == 5:
                        localctx = RAParser.LikeExprContext(self, RAParser.ValExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_valExpr)
                        self.state = 50
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 51
                        self.match(RAParser.LIKE)
                        self.state = 52
                        self.valExpr(7)
                        pass

                    elif la_ == 6:
                        localctx = RAParser.AndExprContext(self, RAParser.ValExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_valExpr)
                        self.state = 53
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 54
                        self.match(RAParser.AND)
                        self.state = 55
                        self.valExpr(3)
                        pass

                    elif la_ == 7:
                        localctx = RAParser.OrExprContext(self, RAParser.ValExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_valExpr)
                        self.state = 56
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 57
                        self.match(RAParser.OR)
                        self.state = 58
                        self.valExpr(2)
                        pass

                    elif la_ == 8:
                        localctx = RAParser.IsNullExprContext(self, RAParser.ValExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_valExpr)
                        self.state = 59
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 60
                        self.match(RAParser.IS_NULL)
                        pass

                    elif la_ == 9:
                        localctx = RAParser.IsNotNullExprContext(self, RAParser.ValExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_valExpr)
                        self.state = 61
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 62
                        self.match(RAParser.IS_NOT_NULL)
                        pass

             
                self.state = 67
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,4,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class ListOfValExprsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def valExpr(self):
            return self.getTypedRuleContext(RAParser.ValExprContext,0)


        def COMMA(self):
            return self.getToken(RAParser.COMMA, 0)

        def listOfValExprs(self):
            return self.getTypedRuleContext(RAParser.ListOfValExprsContext,0)


        def getRuleIndex(self):
            return RAParser.RULE_listOfValExprs

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterListOfValExprs" ):
                listener.enterListOfValExprs(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitListOfValExprs" ):
                listener.exitListOfValExprs(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitListOfValExprs" ):
                return visitor.visitListOfValExprs(self)
            else:
                return visitor.visitChildren(self)




    def listOfValExprs(self):

        localctx = RAParser.ListOfValExprsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_listOfValExprs)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 68
            self.valExpr(0)
            self.state = 71
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==RAParser.COMMA:
                self.state = 69
                self.match(RAParser.COMMA)
                self.state = 70
                self.listOfValExprs()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ListOfIDsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(RAParser.ID, 0)

        def COMMA(self):
            return self.getToken(RAParser.COMMA, 0)

        def listOfIDs(self):
            return self.getTypedRuleContext(RAParser.ListOfIDsContext,0)


        def getRuleIndex(self):
            return RAParser.RULE_listOfIDs

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterListOfIDs" ):
                listener.enterListOfIDs(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitListOfIDs" ):
                listener.exitListOfIDs(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitListOfIDs" ):
                return visitor.visitListOfIDs(self)
            else:
                return visitor.visitChildren(self)




    def listOfIDs(self):

        localctx = RAParser.ListOfIDsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_listOfIDs)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 73
            self.match(RAParser.ID)
            self.state = 76
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==RAParser.COMMA:
                self.state = 74
                self.match(RAParser.COMMA)
                self.state = 75
                self.listOfIDs()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class RelExprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return RAParser.RULE_relExpr

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class FullJoinExprContext(RelExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.RelExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def relExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.RelExprContext)
            else:
                return self.getTypedRuleContext(RAParser.RelExprContext,i)

        def FULL_OUTER_JOIN(self):
            return self.getToken(RAParser.FULL_OUTER_JOIN, 0)
        def ARG_L(self):
            return self.getToken(RAParser.ARG_L, 0)
        def valExpr(self):
            return self.getTypedRuleContext(RAParser.ValExprContext,0)

        def ARG_R(self):
            return self.getToken(RAParser.ARG_R, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFullJoinExpr" ):
                listener.enterFullJoinExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFullJoinExpr" ):
                listener.exitFullJoinExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFullJoinExpr" ):
                return visitor.visitFullJoinExpr(self)
            else:
                return visitor.visitChildren(self)


    class IntersectExprContext(RelExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.RelExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def relExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.RelExprContext)
            else:
                return self.getTypedRuleContext(RAParser.RelExprContext,i)

        def INTERSECT(self):
            return self.getToken(RAParser.INTERSECT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIntersectExpr" ):
                listener.enterIntersectExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIntersectExpr" ):
                listener.exitIntersectExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIntersectExpr" ):
                return visitor.visitIntersectExpr(self)
            else:
                return visitor.visitChildren(self)


    class RightJoinExprContext(RelExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.RelExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def relExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.RelExprContext)
            else:
                return self.getTypedRuleContext(RAParser.RelExprContext,i)

        def RIGHT_OUTER_JOIN(self):
            return self.getToken(RAParser.RIGHT_OUTER_JOIN, 0)
        def ARG_L(self):
            return self.getToken(RAParser.ARG_L, 0)
        def valExpr(self):
            return self.getTypedRuleContext(RAParser.ValExprContext,0)

        def ARG_R(self):
            return self.getToken(RAParser.ARG_R, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRightJoinExpr" ):
                listener.enterRightJoinExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRightJoinExpr" ):
                listener.exitRightJoinExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRightJoinExpr" ):
                return visitor.visitRightJoinExpr(self)
            else:
                return visitor.visitChildren(self)


    class RelExprParenthesizedContext(RelExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.RelExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def PAREN_L(self):
            return self.getToken(RAParser.PAREN_L, 0)
        def relExpr(self):
            return self.getTypedRuleContext(RAParser.RelExprContext,0)

        def PAREN_R(self):
            return self.getToken(RAParser.PAREN_R, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRelExprParenthesized" ):
                listener.enterRelExprParenthesized(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRelExprParenthesized" ):
                listener.exitRelExprParenthesized(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRelExprParenthesized" ):
                return visitor.visitRelExprParenthesized(self)
            else:
                return visitor.visitChildren(self)


    class DiffExprContext(RelExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.RelExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def relExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.RelExprContext)
            else:
                return self.getTypedRuleContext(RAParser.RelExprContext,i)

        def DIFF(self):
            return self.getToken(RAParser.DIFF, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDiffExpr" ):
                listener.enterDiffExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDiffExpr" ):
                listener.exitDiffExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDiffExpr" ):
                return visitor.visitDiffExpr(self)
            else:
                return visitor.visitChildren(self)


    class UnionExprContext(RelExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.RelExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def relExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.RelExprContext)
            else:
                return self.getTypedRuleContext(RAParser.RelExprContext,i)

        def UNION(self):
            return self.getToken(RAParser.UNION, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnionExpr" ):
                listener.enterUnionExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnionExpr" ):
                listener.exitUnionExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnionExpr" ):
                return visitor.visitUnionExpr(self)
            else:
                return visitor.visitChildren(self)


    class RelRefContext(RelExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.RelExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def ID(self):
            return self.getToken(RAParser.ID, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRelRef" ):
                listener.enterRelRef(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRelRef" ):
                listener.exitRelRef(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRelRef" ):
                return visitor.visitRelRef(self)
            else:
                return visitor.visitChildren(self)


    class RenameExprContext(RelExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.RelExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def RENAME(self):
            return self.getToken(RAParser.RENAME, 0)
        def ARG_L(self):
            return self.getToken(RAParser.ARG_L, 0)
        def ARG_R(self):
            return self.getToken(RAParser.ARG_R, 0)
        def relExpr(self):
            return self.getTypedRuleContext(RAParser.RelExprContext,0)

        def listOfIDs(self):
            return self.getTypedRuleContext(RAParser.ListOfIDsContext,0)

        def ID(self):
            return self.getToken(RAParser.ID, 0)
        def COLON(self):
            return self.getToken(RAParser.COLON, 0)
        def STAR(self):
            return self.getToken(RAParser.STAR, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRenameExpr" ):
                listener.enterRenameExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRenameExpr" ):
                listener.exitRenameExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRenameExpr" ):
                return visitor.visitRenameExpr(self)
            else:
                return visitor.visitChildren(self)


    class JoinExprContext(RelExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.RelExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def relExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.RelExprContext)
            else:
                return self.getTypedRuleContext(RAParser.RelExprContext,i)

        def JOIN(self):
            return self.getToken(RAParser.JOIN, 0)
        def ARG_L(self):
            return self.getToken(RAParser.ARG_L, 0)
        def valExpr(self):
            return self.getTypedRuleContext(RAParser.ValExprContext,0)

        def ARG_R(self):
            return self.getToken(RAParser.ARG_R, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterJoinExpr" ):
                listener.enterJoinExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitJoinExpr" ):
                listener.exitJoinExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitJoinExpr" ):
                return visitor.visitJoinExpr(self)
            else:
                return visitor.visitChildren(self)


    class SelectExprContext(RelExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.RelExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SELECT(self):
            return self.getToken(RAParser.SELECT, 0)
        def ARG_L(self):
            return self.getToken(RAParser.ARG_L, 0)
        def valExpr(self):
            return self.getTypedRuleContext(RAParser.ValExprContext,0)

        def ARG_R(self):
            return self.getToken(RAParser.ARG_R, 0)
        def relExpr(self):
            return self.getTypedRuleContext(RAParser.RelExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSelectExpr" ):
                listener.enterSelectExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSelectExpr" ):
                listener.exitSelectExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSelectExpr" ):
                return visitor.visitSelectExpr(self)
            else:
                return visitor.visitChildren(self)


    class CrossExprContext(RelExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.RelExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def relExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.RelExprContext)
            else:
                return self.getTypedRuleContext(RAParser.RelExprContext,i)

        def CROSS(self):
            return self.getToken(RAParser.CROSS, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCrossExpr" ):
                listener.enterCrossExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCrossExpr" ):
                listener.exitCrossExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCrossExpr" ):
                return visitor.visitCrossExpr(self)
            else:
                return visitor.visitChildren(self)


    class AggrExprContext(RelExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.RelExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def AGGR(self):
            return self.getToken(RAParser.AGGR, 0)
        def ARG_L(self):
            return self.getToken(RAParser.ARG_L, 0)
        def listOfValExprs(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.ListOfValExprsContext)
            else:
                return self.getTypedRuleContext(RAParser.ListOfValExprsContext,i)

        def ARG_R(self):
            return self.getToken(RAParser.ARG_R, 0)
        def relExpr(self):
            return self.getTypedRuleContext(RAParser.RelExprContext,0)

        def COLON(self):
            return self.getToken(RAParser.COLON, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAggrExpr" ):
                listener.enterAggrExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAggrExpr" ):
                listener.exitAggrExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAggrExpr" ):
                return visitor.visitAggrExpr(self)
            else:
                return visitor.visitChildren(self)


    class ProjectExprContext(RelExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.RelExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def PROJECT(self):
            return self.getToken(RAParser.PROJECT, 0)
        def ARG_L(self):
            return self.getToken(RAParser.ARG_L, 0)
        def listOfValExprs(self):
            return self.getTypedRuleContext(RAParser.ListOfValExprsContext,0)

        def ARG_R(self):
            return self.getToken(RAParser.ARG_R, 0)
        def relExpr(self):
            return self.getTypedRuleContext(RAParser.RelExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProjectExpr" ):
                listener.enterProjectExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProjectExpr" ):
                listener.exitProjectExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProjectExpr" ):
                return visitor.visitProjectExpr(self)
            else:
                return visitor.visitChildren(self)


    class LeftJoinExprContext(RelExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.RelExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def relExpr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.RelExprContext)
            else:
                return self.getTypedRuleContext(RAParser.RelExprContext,i)

        def LEFT_OUTER_JOIN(self):
            return self.getToken(RAParser.LEFT_OUTER_JOIN, 0)
        def ARG_L(self):
            return self.getToken(RAParser.ARG_L, 0)
        def valExpr(self):
            return self.getTypedRuleContext(RAParser.ValExprContext,0)

        def ARG_R(self):
            return self.getToken(RAParser.ARG_R, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLeftJoinExpr" ):
                listener.enterLeftJoinExpr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLeftJoinExpr" ):
                listener.exitLeftJoinExpr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLeftJoinExpr" ):
                return visitor.visitLeftJoinExpr(self)
            else:
                return visitor.visitChildren(self)



    def relExpr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = RAParser.RelExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 6
        self.enterRecursionRule(localctx, 6, self.RULE_relExpr, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 119
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [RAParser.PAREN_L]:
                localctx = RAParser.RelExprParenthesizedContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 79
                self.match(RAParser.PAREN_L)
                self.state = 80
                self.relExpr(0)
                self.state = 81
                self.match(RAParser.PAREN_R)
                pass
            elif token in [RAParser.ID]:
                localctx = RAParser.RelRefContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 83
                self.match(RAParser.ID)
                pass
            elif token in [RAParser.RENAME]:
                localctx = RAParser.RenameExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 84
                self.match(RAParser.RENAME)
                self.state = 85
                self.match(RAParser.ARG_L)
                self.state = 93
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,8,self._ctx)
                if la_ == 1:
                    self.state = 86
                    self.match(RAParser.ID)
                    self.state = 87
                    self.match(RAParser.COLON)
                    self.state = 90
                    self._errHandler.sync(self)
                    token = self._input.LA(1)
                    if token in [RAParser.STAR]:
                        self.state = 88
                        self.match(RAParser.STAR)
                        pass
                    elif token in [RAParser.ID]:
                        self.state = 89
                        self.listOfIDs()
                        pass
                    else:
                        raise NoViableAltException(self)

                    pass

                elif la_ == 2:
                    self.state = 92
                    self.listOfIDs()
                    pass


                self.state = 95
                self.match(RAParser.ARG_R)
                self.state = 96
                self.relExpr(12)
                pass
            elif token in [RAParser.PROJECT]:
                localctx = RAParser.ProjectExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 97
                self.match(RAParser.PROJECT)
                self.state = 98
                self.match(RAParser.ARG_L)
                self.state = 99
                self.listOfValExprs()
                self.state = 100
                self.match(RAParser.ARG_R)
                self.state = 101
                self.relExpr(11)
                pass
            elif token in [RAParser.SELECT]:
                localctx = RAParser.SelectExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 103
                self.match(RAParser.SELECT)
                self.state = 104
                self.match(RAParser.ARG_L)
                self.state = 105
                self.valExpr(0)
                self.state = 106
                self.match(RAParser.ARG_R)
                self.state = 107
                self.relExpr(10)
                pass
            elif token in [RAParser.AGGR]:
                localctx = RAParser.AggrExprContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 109
                self.match(RAParser.AGGR)
                self.state = 110
                self.match(RAParser.ARG_L)
                self.state = 111
                self.listOfValExprs()
                self.state = 114
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==RAParser.COLON:
                    self.state = 112
                    self.match(RAParser.COLON)
                    self.state = 113
                    self.listOfValExprs()


                self.state = 116
                self.match(RAParser.ARG_R)
                self.state = 117
                self.relExpr(1)
                pass
            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 171
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,16,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 169
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,15,self._ctx)
                    if la_ == 1:
                        localctx = RAParser.JoinExprContext(self, RAParser.RelExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_relExpr)
                        self.state = 121
                        if not self.precpred(self._ctx, 9):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 9)")
                        self.state = 122
                        self.match(RAParser.JOIN)
                        self.state = 127
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        if _la==RAParser.ARG_L:
                            self.state = 123
                            self.match(RAParser.ARG_L)
                            self.state = 124
                            self.valExpr(0)
                            self.state = 125
                            self.match(RAParser.ARG_R)


                        self.state = 129
                        self.relExpr(10)
                        pass

                    elif la_ == 2:
                        localctx = RAParser.LeftJoinExprContext(self, RAParser.RelExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_relExpr)
                        self.state = 130
                        if not self.precpred(self._ctx, 8):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 8)")
                        self.state = 131
                        self.match(RAParser.LEFT_OUTER_JOIN)
                        self.state = 136
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        if _la==RAParser.ARG_L:
                            self.state = 132
                            self.match(RAParser.ARG_L)
                            self.state = 133
                            self.valExpr(0)
                            self.state = 134
                            self.match(RAParser.ARG_R)


                        self.state = 138
                        self.relExpr(9)
                        pass

                    elif la_ == 3:
                        localctx = RAParser.RightJoinExprContext(self, RAParser.RelExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_relExpr)
                        self.state = 139
                        if not self.precpred(self._ctx, 7):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 7)")
                        self.state = 140
                        self.match(RAParser.RIGHT_OUTER_JOIN)
                        self.state = 145
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        if _la==RAParser.ARG_L:
                            self.state = 141
                            self.match(RAParser.ARG_L)
                            self.state = 142
                            self.valExpr(0)
                            self.state = 143
                            self.match(RAParser.ARG_R)


                        self.state = 147
                        self.relExpr(8)
                        pass

                    elif la_ == 4:
                        localctx = RAParser.FullJoinExprContext(self, RAParser.RelExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_relExpr)
                        self.state = 148
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 149
                        self.match(RAParser.FULL_OUTER_JOIN)
                        self.state = 154
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)
                        if _la==RAParser.ARG_L:
                            self.state = 150
                            self.match(RAParser.ARG_L)
                            self.state = 151
                            self.valExpr(0)
                            self.state = 152
                            self.match(RAParser.ARG_R)


                        self.state = 156
                        self.relExpr(7)
                        pass

                    elif la_ == 5:
                        localctx = RAParser.CrossExprContext(self, RAParser.RelExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_relExpr)
                        self.state = 157
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 158
                        self.match(RAParser.CROSS)
                        self.state = 159
                        self.relExpr(6)
                        pass

                    elif la_ == 6:
                        localctx = RAParser.UnionExprContext(self, RAParser.RelExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_relExpr)
                        self.state = 160
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 161
                        self.match(RAParser.UNION)
                        self.state = 162
                        self.relExpr(5)
                        pass

                    elif la_ == 7:
                        localctx = RAParser.DiffExprContext(self, RAParser.RelExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_relExpr)
                        self.state = 163
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 164
                        self.match(RAParser.DIFF)
                        self.state = 165
                        self.relExpr(4)
                        pass

                    elif la_ == 8:
                        localctx = RAParser.IntersectExprContext(self, RAParser.RelExprContext(self, _parentctx, _parentState))
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_relExpr)
                        self.state = 166
                        if not self.precpred(self._ctx, 2):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                        self.state = 167
                        self.match(RAParser.INTERSECT)
                        self.state = 168
                        self.relExpr(3)
                        pass

             
                self.state = 173
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,16,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class DefinitionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(RAParser.ID, 0)

        def GETS(self):
            return self.getToken(RAParser.GETS, 0)

        def relExpr(self):
            return self.getTypedRuleContext(RAParser.RelExprContext,0)


        def getRuleIndex(self):
            return RAParser.RULE_definition

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDefinition" ):
                listener.enterDefinition(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDefinition" ):
                listener.exitDefinition(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDefinition" ):
                return visitor.visitDefinition(self)
            else:
                return visitor.visitChildren(self)




    def definition(self):

        localctx = RAParser.DefinitionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_definition)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 174
            self.match(RAParser.ID)
            self.state = 175
            self.match(RAParser.GETS)
            self.state = 176
            self.relExpr(0)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CommandContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return RAParser.RULE_command

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class ListCommandContext(CommandContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.CommandContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def LIST(self):
            return self.getToken(RAParser.LIST, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterListCommand" ):
                listener.enterListCommand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitListCommand" ):
                listener.exitListCommand(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitListCommand" ):
                return visitor.visitListCommand(self)
            else:
                return visitor.visitChildren(self)


    class ClearCommandContext(CommandContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.CommandContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def CLEAR(self):
            return self.getToken(RAParser.CLEAR, 0)
        def ID(self):
            return self.getToken(RAParser.ID, 0)
        def STAR(self):
            return self.getToken(RAParser.STAR, 0)
        def FORCE(self):
            return self.getToken(RAParser.FORCE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterClearCommand" ):
                listener.enterClearCommand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitClearCommand" ):
                listener.exitClearCommand(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitClearCommand" ):
                return visitor.visitClearCommand(self)
            else:
                return visitor.visitChildren(self)


    class QuitCommandContext(CommandContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.CommandContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def QUIT(self):
            return self.getToken(RAParser.QUIT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterQuitCommand" ):
                listener.enterQuitCommand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitQuitCommand" ):
                listener.exitQuitCommand(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitQuitCommand" ):
                return visitor.visitQuitCommand(self)
            else:
                return visitor.visitChildren(self)


    class SourceCommandContext(CommandContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.CommandContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SOURCE(self):
            return self.getToken(RAParser.SOURCE, 0)
        def STRING(self):
            return self.getToken(RAParser.STRING, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSourceCommand" ):
                listener.enterSourceCommand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSourceCommand" ):
                listener.exitSourceCommand(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSourceCommand" ):
                return visitor.visitSourceCommand(self)
            else:
                return visitor.visitChildren(self)


    class SaveCommandContext(CommandContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.CommandContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SAVE(self):
            return self.getToken(RAParser.SAVE, 0)
        def ID(self):
            return self.getToken(RAParser.ID, 0)
        def STAR(self):
            return self.getToken(RAParser.STAR, 0)
        def FORCE(self):
            return self.getToken(RAParser.FORCE, 0)
        def STRING(self):
            return self.getToken(RAParser.STRING, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSaveCommand" ):
                listener.enterSaveCommand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSaveCommand" ):
                listener.exitSaveCommand(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSaveCommand" ):
                return visitor.visitSaveCommand(self)
            else:
                return visitor.visitChildren(self)


    class SqlexecCommandContext(CommandContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a RAParser.CommandContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SQLEXEC(self):
            return self.getToken(RAParser.SQLEXEC, 0)
        def SQLEXEC_TEXT(self):
            return self.getToken(RAParser.SQLEXEC_TEXT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSqlexecCommand" ):
                listener.enterSqlexecCommand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSqlexecCommand" ):
                listener.exitSqlexecCommand(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitSqlexecCommand" ):
                return visitor.visitSqlexecCommand(self)
            else:
                return visitor.visitChildren(self)



    def command(self):

        localctx = RAParser.CommandContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_command)
        self._la = 0 # Token type
        try:
            self.state = 200
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [RAParser.LIST]:
                localctx = RAParser.ListCommandContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 178
                self.match(RAParser.LIST)
                pass
            elif token in [RAParser.CLEAR]:
                localctx = RAParser.ClearCommandContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 179
                self.match(RAParser.CLEAR)
                self.state = 185
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [RAParser.FORCE, RAParser.ID]:
                    self.state = 181
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if _la==RAParser.FORCE:
                        self.state = 180
                        self.match(RAParser.FORCE)


                    self.state = 183
                    self.match(RAParser.ID)
                    pass
                elif token in [RAParser.STAR]:
                    self.state = 184
                    self.match(RAParser.STAR)
                    pass
                else:
                    raise NoViableAltException(self)

                pass
            elif token in [RAParser.SAVE]:
                localctx = RAParser.SaveCommandContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 187
                self.match(RAParser.SAVE)
                self.state = 189
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==RAParser.FORCE:
                    self.state = 188
                    self.match(RAParser.FORCE)


                self.state = 191
                _la = self._input.LA(1)
                if not(_la==RAParser.ID or _la==RAParser.STAR):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 193
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==RAParser.STRING:
                    self.state = 192
                    self.match(RAParser.STRING)


                pass
            elif token in [RAParser.SOURCE]:
                localctx = RAParser.SourceCommandContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 195
                self.match(RAParser.SOURCE)
                self.state = 196
                self.match(RAParser.STRING)
                pass
            elif token in [RAParser.QUIT]:
                localctx = RAParser.QuitCommandContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 197
                self.match(RAParser.QUIT)
                pass
            elif token in [RAParser.SQLEXEC]:
                localctx = RAParser.SqlexecCommandContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 198
                self.match(RAParser.SQLEXEC)
                self.state = 199
                self.match(RAParser.SQLEXEC_TEXT)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TERMINATOR(self):
            return self.getToken(RAParser.TERMINATOR, 0)

        def relExpr(self):
            return self.getTypedRuleContext(RAParser.RelExprContext,0)


        def definition(self):
            return self.getTypedRuleContext(RAParser.DefinitionContext,0)


        def command(self):
            return self.getTypedRuleContext(RAParser.CommandContext,0)


        def getRuleIndex(self):
            return RAParser.RULE_statement

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStatement" ):
                listener.enterStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStatement" ):
                listener.exitStatement(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStatement" ):
                return visitor.visitStatement(self)
            else:
                return visitor.visitChildren(self)




    def statement(self):

        localctx = RAParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 205
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,22,self._ctx)
            if la_ == 1:
                self.state = 202
                self.relExpr(0)
                pass

            elif la_ == 2:
                self.state = 203
                self.definition()
                pass

            elif la_ == 3:
                self.state = 204
                self.command()
                pass


            self.state = 207
            self.match(RAParser.TERMINATOR)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(RAParser.EOF, 0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(RAParser.StatementContext)
            else:
                return self.getTypedRuleContext(RAParser.StatementContext,i)


        def getRuleIndex(self):
            return RAParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = RAParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 212
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << RAParser.ID) | (1 << RAParser.RENAME) | (1 << RAParser.PROJECT) | (1 << RAParser.SELECT) | (1 << RAParser.AGGR) | (1 << RAParser.PAREN_L) | (1 << RAParser.LIST) | (1 << RAParser.CLEAR) | (1 << RAParser.SAVE) | (1 << RAParser.SOURCE) | (1 << RAParser.QUIT) | (1 << RAParser.SQLEXEC))) != 0):
                self.state = 209
                self.statement()
                self.state = 214
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 215
            self.match(RAParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[0] = self.valExpr_sempred
        self._predicates[3] = self.relExpr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def valExpr_sempred(self, localctx:ValExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 10)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 9)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 8)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 7)
         

            if predIndex == 4:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 5:
                return self.precpred(self._ctx, 2)
         

            if predIndex == 6:
                return self.precpred(self._ctx, 1)
         

            if predIndex == 7:
                return self.precpred(self._ctx, 5)
         

            if predIndex == 8:
                return self.precpred(self._ctx, 4)
         

    def relExpr_sempred(self, localctx:RelExprContext, predIndex:int):
            if predIndex == 9:
                return self.precpred(self._ctx, 9)
         

            if predIndex == 10:
                return self.precpred(self._ctx, 8)
         

            if predIndex == 11:
                return self.precpred(self._ctx, 7)
         

            if predIndex == 12:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 13:
                return self.precpred(self._ctx, 5)
         

            if predIndex == 14:
                return self.precpred(self._ctx, 4)
         

            if predIndex == 15:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 16:
                return self.precpred(self._ctx, 2)
         




