# Generated from C:/Users/achio/Documents/AnulIII/Semestrul2/licenta/djangoapp/ratosql/parser/radb\RALexer.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\28")
        buf.write("\u0242\b\1\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6")
        buf.write("\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r")
        buf.write("\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22")
        buf.write("\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30")
        buf.write("\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35")
        buf.write("\4\36\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4")
        buf.write("%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t")
        buf.write("-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63")
        buf.write("\4\64\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4")
        buf.write(":\t:\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4")
        buf.write("C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I\tI\4J\tJ\4K\tK\4")
        buf.write("L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\3\2\6\2\u00a8")
        buf.write("\n\2\r\2\16\2\u00a9\3\2\3\2\3\3\3\3\3\3\3\3\3\3\7\3\u00b3")
        buf.write("\n\3\f\3\16\3\u00b6\13\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3")
        buf.write("\4\3\4\7\4\u00c1\n\4\f\4\16\4\u00c4\13\4\3\4\3\4\3\4\3")
        buf.write("\4\3\5\3\5\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b")
        buf.write("\3\b\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3")
        buf.write("\13\3\13\3\f\3\f\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\16\3")
        buf.write("\16\3\17\3\17\3\17\3\17\3\17\5\17\u00f5\n\17\3\17\7\17")
        buf.write("\u00f8\n\17\f\17\16\17\u00fb\13\17\3\17\3\17\3\20\7\20")
        buf.write("\u0100\n\20\f\20\16\20\u0103\13\20\3\20\5\20\u0106\n\20")
        buf.write("\3\20\6\20\u0109\n\20\r\20\16\20\u010a\3\21\3\21\7\21")
        buf.write("\u010f\n\21\f\21\16\21\u0112\13\21\3\22\3\22\3\22\3\22")
        buf.write("\3\22\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23")
        buf.write("\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\25")
        buf.write("\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\26")
        buf.write("\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26")
        buf.write("\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\27")
        buf.write("\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30")
        buf.write("\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30\3\30")
        buf.write("\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\32\3\32")
        buf.write("\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\33")
        buf.write("\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\34")
        buf.write("\3\35\3\35\3\35\3\35\3\35\3\35\3\36\3\36\3\37\3\37\3 ")
        buf.write("\3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3$\3%\3%\3&\3&\3\'\3\'\3")
        buf.write("\'\3(\3(\3)\3)\3)\3*\3*\3+\3+\3+\3,\3,\3,\3-\3-\3-\3.")
        buf.write("\3.\3/\3/\3\60\3\60\3\61\3\61\3\61\3\61\3\61\3\61\3\62")
        buf.write("\3\62\3\62\3\62\3\62\3\62\3\62\3\63\3\63\3\63\3\63\3\63")
        buf.write("\3\63\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\65\3\65")
        buf.write("\3\65\3\65\3\65\3\65\3\66\3\66\3\66\3\66\3\66\3\66\3\66")
        buf.write("\3\66\3\66\3\66\3\66\3\67\3\67\38\38\39\39\3:\3:\3;\3")
        buf.write(";\3<\3<\3=\3=\3>\3>\3?\3?\3@\3@\3A\3A\3B\3B\3C\3C\3D\3")
        buf.write("D\3E\3E\3F\3F\3G\3G\3H\3H\3I\3I\3J\3J\3K\3K\3L\3L\3M\3")
        buf.write("M\3N\3N\3O\3O\3P\3P\3Q\6Q\u0219\nQ\rQ\16Q\u021a\3R\5R")
        buf.write("\u021e\nR\3R\3R\3R\3R\5R\u0224\nR\3R\7R\u0227\nR\fR\16")
        buf.write("R\u022a\13R\3R\3R\3R\3R\5R\u0230\nR\3R\7R\u0233\nR\fR")
        buf.write("\16R\u0236\13R\3R\3R\7R\u023a\nR\fR\16R\u023d\13R\3R\3")
        buf.write("R\3R\3R\5\u00b4\u00c2\u023b\2S\4\3\6\4\b\5\n\6\f\7\16")
        buf.write("\b\20\t\22\n\24\13\26\f\30\r\32\16\34\17\36\20 \21\"\22")
        buf.write("$\23&\24(\25*\26,\27.\30\60\31\62\32\64\33\66\348\35:")
        buf.write("\36<\37> @!B\"D#F$H%J&L\'N(P)R*T+V,X-Z.\\/^\60`\61b\62")
        buf.write("d\63f\64h\65j\66l\67n\2p\2r\2t\2v\2x\2z\2|\2~\2\u0080")
        buf.write("\2\u0082\2\u0084\2\u0086\2\u0088\2\u008a\2\u008c\2\u008e")
        buf.write("\2\u0090\2\u0092\2\u0094\2\u0096\2\u0098\2\u009a\2\u009c")
        buf.write("\2\u009e\2\u00a0\2\u00a2\2\u00a48\4\2\3\"\5\2\13\f\17")
        buf.write("\17\"\"\5\2\f\f\17\17))\5\2C\\aac|\6\2\62;C\\aac|\4\2")
        buf.write("CCcc\4\2DDdd\4\2EEee\4\2FFff\4\2GGgg\4\2HHhh\4\2IIii\4")
        buf.write("\2JJjj\4\2KKkk\4\2LLll\4\2MMmm\4\2NNnn\4\2OOoo\4\2PPp")
        buf.write("p\4\2QQqq\4\2RRrr\4\2SSss\4\2TTtt\4\2UUuu\4\2VVvv\4\2")
        buf.write("WWww\4\2XXxx\4\2YYyy\4\2ZZzz\4\2[[{{\4\2\\\\||\5\2\f\f")
        buf.write("\17\17$$\4\2$$))\2\u023c\2\4\3\2\2\2\2\6\3\2\2\2\2\b\3")
        buf.write("\2\2\2\2\n\3\2\2\2\2\f\3\2\2\2\2\16\3\2\2\2\2\20\3\2\2")
        buf.write("\2\2\22\3\2\2\2\2\24\3\2\2\2\2\26\3\2\2\2\2\30\3\2\2\2")
        buf.write("\2\32\3\2\2\2\2\34\3\2\2\2\2\36\3\2\2\2\2 \3\2\2\2\2\"")
        buf.write("\3\2\2\2\2$\3\2\2\2\2&\3\2\2\2\2(\3\2\2\2\2*\3\2\2\2\2")
        buf.write(",\3\2\2\2\2.\3\2\2\2\2\60\3\2\2\2\2\62\3\2\2\2\2\64\3")
        buf.write("\2\2\2\2\66\3\2\2\2\28\3\2\2\2\2:\3\2\2\2\2<\3\2\2\2\2")
        buf.write(">\3\2\2\2\2@\3\2\2\2\2B\3\2\2\2\2D\3\2\2\2\2F\3\2\2\2")
        buf.write("\2H\3\2\2\2\2J\3\2\2\2\2L\3\2\2\2\2N\3\2\2\2\2P\3\2\2")
        buf.write("\2\2R\3\2\2\2\2T\3\2\2\2\2V\3\2\2\2\2X\3\2\2\2\2Z\3\2")
        buf.write("\2\2\2\\\3\2\2\2\2^\3\2\2\2\2`\3\2\2\2\2b\3\2\2\2\2d\3")
        buf.write("\2\2\2\2f\3\2\2\2\2h\3\2\2\2\2j\3\2\2\2\2l\3\2\2\2\3\u00a4")
        buf.write("\3\2\2\2\4\u00a7\3\2\2\2\6\u00ad\3\2\2\2\b\u00bc\3\2\2")
        buf.write("\2\n\u00c9\3\2\2\2\f\u00cb\3\2\2\2\16\u00cd\3\2\2\2\20")
        buf.write("\u00d1\3\2\2\2\22\u00d7\3\2\2\2\24\u00da\3\2\2\2\26\u00df")
        buf.write("\3\2\2\2\30\u00e4\3\2\2\2\32\u00e8\3\2\2\2\34\u00eb\3")
        buf.write("\2\2\2\36\u00ef\3\2\2\2 \u0101\3\2\2\2\"\u010c\3\2\2\2")
        buf.write("$\u0113\3\2\2\2&\u011b\3\2\2\2(\u0124\3\2\2\2*\u012c\3")
        buf.write("\2\2\2,\u0132\3\2\2\2.\u0143\3\2\2\2\60\u0155\3\2\2\2")
        buf.write("\62\u0166\3\2\2\2\64\u016d\3\2\2\2\66\u0174\3\2\2\28\u017a")
        buf.write("\3\2\2\2:\u0185\3\2\2\2<\u018b\3\2\2\2>\u018d\3\2\2\2")
        buf.write("@\u018f\3\2\2\2B\u0191\3\2\2\2D\u0193\3\2\2\2F\u0195\3")
        buf.write("\2\2\2H\u0197\3\2\2\2J\u019a\3\2\2\2L\u019c\3\2\2\2N\u019e")
        buf.write("\3\2\2\2P\u01a1\3\2\2\2R\u01a3\3\2\2\2T\u01a6\3\2\2\2")
        buf.write("V\u01a8\3\2\2\2X\u01ab\3\2\2\2Z\u01ae\3\2\2\2\\\u01b1")
        buf.write("\3\2\2\2^\u01b3\3\2\2\2`\u01b5\3\2\2\2b\u01b7\3\2\2\2")
        buf.write("d\u01bd\3\2\2\2f\u01c4\3\2\2\2h\u01ca\3\2\2\2j\u01d2\3")
        buf.write("\2\2\2l\u01d8\3\2\2\2n\u01e3\3\2\2\2p\u01e5\3\2\2\2r\u01e7")
        buf.write("\3\2\2\2t\u01e9\3\2\2\2v\u01eb\3\2\2\2x\u01ed\3\2\2\2")
        buf.write("z\u01ef\3\2\2\2|\u01f1\3\2\2\2~\u01f3\3\2\2\2\u0080\u01f5")
        buf.write("\3\2\2\2\u0082\u01f7\3\2\2\2\u0084\u01f9\3\2\2\2\u0086")
        buf.write("\u01fb\3\2\2\2\u0088\u01fd\3\2\2\2\u008a\u01ff\3\2\2\2")
        buf.write("\u008c\u0201\3\2\2\2\u008e\u0203\3\2\2\2\u0090\u0205\3")
        buf.write("\2\2\2\u0092\u0207\3\2\2\2\u0094\u0209\3\2\2\2\u0096\u020b")
        buf.write("\3\2\2\2\u0098\u020d\3\2\2\2\u009a\u020f\3\2\2\2\u009c")
        buf.write("\u0211\3\2\2\2\u009e\u0213\3\2\2\2\u00a0\u0215\3\2\2\2")
        buf.write("\u00a2\u0218\3\2\2\2\u00a4\u021d\3\2\2\2\u00a6\u00a8\t")
        buf.write("\2\2\2\u00a7\u00a6\3\2\2\2\u00a8\u00a9\3\2\2\2\u00a9\u00a7")
        buf.write("\3\2\2\2\u00a9\u00aa\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab")
        buf.write("\u00ac\b\2\2\2\u00ac\5\3\2\2\2\u00ad\u00ae\7\61\2\2\u00ae")
        buf.write("\u00af\7,\2\2\u00af\u00b4\3\2\2\2\u00b0\u00b3\5\6\3\2")
        buf.write("\u00b1\u00b3\13\2\2\2\u00b2\u00b0\3\2\2\2\u00b2\u00b1")
        buf.write("\3\2\2\2\u00b3\u00b6\3\2\2\2\u00b4\u00b5\3\2\2\2\u00b4")
        buf.write("\u00b2\3\2\2\2\u00b5\u00b7\3\2\2\2\u00b6\u00b4\3\2\2\2")
        buf.write("\u00b7\u00b8\7,\2\2\u00b8\u00b9\7\61\2\2\u00b9\u00ba\3")
        buf.write("\2\2\2\u00ba\u00bb\b\3\2\2\u00bb\7\3\2\2\2\u00bc\u00bd")
        buf.write("\7\61\2\2\u00bd\u00be\7\61\2\2\u00be\u00c2\3\2\2\2\u00bf")
        buf.write("\u00c1\13\2\2\2\u00c0\u00bf\3\2\2\2\u00c1\u00c4\3\2\2")
        buf.write("\2\u00c2\u00c3\3\2\2\2\u00c2\u00c0\3\2\2\2\u00c3\u00c5")
        buf.write("\3\2\2\2\u00c4\u00c2\3\2\2\2\u00c5\u00c6\7\f\2\2\u00c6")
        buf.write("\u00c7\3\2\2\2\u00c7\u00c8\b\4\2\2\u00c8\t\3\2\2\2\u00c9")
        buf.write("\u00ca\7=\2\2\u00ca\13\3\2\2\2\u00cb\u00cc\7#\2\2\u00cc")
        buf.write("\r\3\2\2\2\u00cd\u00ce\5\22\t\2\u00ce\u00cf\5\4\2\2\u00cf")
        buf.write("\u00d0\5\24\n\2\u00d0\17\3\2\2\2\u00d1\u00d2\5\22\t\2")
        buf.write("\u00d2\u00d3\5\4\2\2\u00d3\u00d4\5\34\16\2\u00d4\u00d5")
        buf.write("\5\4\2\2\u00d5\u00d6\5\24\n\2\u00d6\21\3\2\2\2\u00d7\u00d8")
        buf.write("\5~?\2\u00d8\u00d9\5\u0092I\2\u00d9\23\3\2\2\2\u00da\u00db")
        buf.write("\5\u0088D\2\u00db\u00dc\5\u0096K\2\u00dc\u00dd\5\u0084")
        buf.write("B\2\u00dd\u00de\5\u0084B\2\u00de\25\3\2\2\2\u00df\u00e0")
        buf.write("\5\u0084B\2\u00e0\u00e1\5~?\2\u00e1\u00e2\5\u0082A\2\u00e2")
        buf.write("\u00e3\5v;\2\u00e3\27\3\2\2\2\u00e4\u00e5\5n\67\2\u00e5")
        buf.write("\u00e6\5\u0088D\2\u00e6\u00e7\5t:\2\u00e7\31\3\2\2\2\u00e8")
        buf.write("\u00e9\5\u008aE\2\u00e9\u00ea\5\u0090H\2\u00ea\33\3\2")
        buf.write("\2\2\u00eb\u00ec\5\u0088D\2\u00ec\u00ed\5\u008aE\2\u00ed")
        buf.write("\u00ee\5\u0094J\2\u00ee\35\3\2\2\2\u00ef\u00f9\7)\2\2")
        buf.write("\u00f0\u00f8\n\3\2\2\u00f1\u00f2\7)\2\2\u00f2\u00f8\7")
        buf.write(")\2\2\u00f3\u00f5\7\17\2\2\u00f4\u00f3\3\2\2\2\u00f4\u00f5")
        buf.write("\3\2\2\2\u00f5\u00f6\3\2\2\2\u00f6\u00f8\7\f\2\2\u00f7")
        buf.write("\u00f0\3\2\2\2\u00f7\u00f1\3\2\2\2\u00f7\u00f4\3\2\2\2")
        buf.write("\u00f8\u00fb\3\2\2\2\u00f9\u00f7\3\2\2\2\u00f9\u00fa\3")
        buf.write("\2\2\2\u00fa\u00fc\3\2\2\2\u00fb\u00f9\3\2\2\2\u00fc\u00fd")
        buf.write("\7)\2\2\u00fd\37\3\2\2\2\u00fe\u0100\5\u00a2Q\2\u00ff")
        buf.write("\u00fe\3\2\2\2\u0100\u0103\3\2\2\2\u0101\u00ff\3\2\2\2")
        buf.write("\u0101\u0102\3\2\2\2\u0102\u0105\3\2\2\2\u0103\u0101\3")
        buf.write("\2\2\2\u0104\u0106\7\60\2\2\u0105\u0104\3\2\2\2\u0105")
        buf.write("\u0106\3\2\2\2\u0106\u0108\3\2\2\2\u0107\u0109\5\u00a2")
        buf.write("Q\2\u0108\u0107\3\2\2\2\u0109\u010a\3\2\2\2\u010a\u0108")
        buf.write("\3\2\2\2\u010a\u010b\3\2\2\2\u010b!\3\2\2\2\u010c\u0110")
        buf.write("\t\4\2\2\u010d\u010f\t\5\2\2\u010e\u010d\3\2\2\2\u010f")
        buf.write("\u0112\3\2\2\2\u0110\u010e\3\2\2\2\u0110\u0111\3\2\2\2")
        buf.write("\u0111#\3\2\2\2\u0112\u0110\3\2\2\2\u0113\u0114\7^\2\2")
        buf.write("\u0114\u0115\7t\2\2\u0115\u0116\7g\2\2\u0116\u0117\7p")
        buf.write("\2\2\u0117\u0118\7c\2\2\u0118\u0119\7o\2\2\u0119\u011a")
        buf.write("\7g\2\2\u011a%\3\2\2\2\u011b\u011c\7^\2\2\u011c\u011d")
        buf.write("\7r\2\2\u011d\u011e\7t\2\2\u011e\u011f\7q\2\2\u011f\u0120")
        buf.write("\7l\2\2\u0120\u0121\7g\2\2\u0121\u0122\7e\2\2\u0122\u0123")
        buf.write("\7v\2\2\u0123\'\3\2\2\2\u0124\u0125\7^\2\2\u0125\u0126")
        buf.write("\7u\2\2\u0126\u0127\7g\2\2\u0127\u0128\7n\2\2\u0128\u0129")
        buf.write("\7g\2\2\u0129\u012a\7e\2\2\u012a\u012b\7v\2\2\u012b)\3")
        buf.write("\2\2\2\u012c\u012d\7^\2\2\u012d\u012e\7l\2\2\u012e\u012f")
        buf.write("\7q\2\2\u012f\u0130\7k\2\2\u0130\u0131\7p\2\2\u0131+\3")
        buf.write("\2\2\2\u0132\u0133\7^\2\2\u0133\u0134\7n\2\2\u0134\u0135")
        buf.write("\7g\2\2\u0135\u0136\7h\2\2\u0136\u0137\7v\2\2\u0137\u0138")
        buf.write("\7a\2\2\u0138\u0139\7q\2\2\u0139\u013a\7w\2\2\u013a\u013b")
        buf.write("\7v\2\2\u013b\u013c\7g\2\2\u013c\u013d\7t\2\2\u013d\u013e")
        buf.write("\7a\2\2\u013e\u013f\7l\2\2\u013f\u0140\7q\2\2\u0140\u0141")
        buf.write("\7k\2\2\u0141\u0142\7p\2\2\u0142-\3\2\2\2\u0143\u0144")
        buf.write("\7^\2\2\u0144\u0145\7t\2\2\u0145\u0146\7k\2\2\u0146\u0147")
        buf.write("\7i\2\2\u0147\u0148\7j\2\2\u0148\u0149\7v\2\2\u0149\u014a")
        buf.write("\7a\2\2\u014a\u014b\7q\2\2\u014b\u014c\7w\2\2\u014c\u014d")
        buf.write("\7v\2\2\u014d\u014e\7g\2\2\u014e\u014f\7t\2\2\u014f\u0150")
        buf.write("\7a\2\2\u0150\u0151\7l\2\2\u0151\u0152\7q\2\2\u0152\u0153")
        buf.write("\7k\2\2\u0153\u0154\7p\2\2\u0154/\3\2\2\2\u0155\u0156")
        buf.write("\7^\2\2\u0156\u0157\7h\2\2\u0157\u0158\7w\2\2\u0158\u0159")
        buf.write("\7n\2\2\u0159\u015a\7n\2\2\u015a\u015b\7a\2\2\u015b\u015c")
        buf.write("\7q\2\2\u015c\u015d\7w\2\2\u015d\u015e\7v\2\2\u015e\u015f")
        buf.write("\7g\2\2\u015f\u0160\7t\2\2\u0160\u0161\7a\2\2\u0161\u0162")
        buf.write("\7l\2\2\u0162\u0163\7q\2\2\u0163\u0164\7k\2\2\u0164\u0165")
        buf.write("\7p\2\2\u0165\61\3\2\2\2\u0166\u0167\7^\2\2\u0167\u0168")
        buf.write("\7e\2\2\u0168\u0169\7t\2\2\u0169\u016a\7q\2\2\u016a\u016b")
        buf.write("\7u\2\2\u016b\u016c\7u\2\2\u016c\63\3\2\2\2\u016d\u016e")
        buf.write("\7^\2\2\u016e\u016f\7w\2\2\u016f\u0170\7p\2\2\u0170\u0171")
        buf.write("\7k\2\2\u0171\u0172\7q\2\2\u0172\u0173\7p\2\2\u0173\65")
        buf.write("\3\2\2\2\u0174\u0175\7^\2\2\u0175\u0176\7f\2\2\u0176\u0177")
        buf.write("\7k\2\2\u0177\u0178\7h\2\2\u0178\u0179\7h\2\2\u0179\67")
        buf.write("\3\2\2\2\u017a\u017b\7^\2\2\u017b\u017c\7k\2\2\u017c\u017d")
        buf.write("\7p\2\2\u017d\u017e\7v\2\2\u017e\u017f\7g\2\2\u017f\u0180")
        buf.write("\7t\2\2\u0180\u0181\7u\2\2\u0181\u0182\7g\2\2\u0182\u0183")
        buf.write("\7e\2\2\u0183\u0184\7v\2\2\u01849\3\2\2\2\u0185\u0186")
        buf.write("\7^\2\2\u0186\u0187\7c\2\2\u0187\u0188\7i\2\2\u0188\u0189")
        buf.write("\7i\2\2\u0189\u018a\7t\2\2\u018a;\3\2\2\2\u018b\u018c")
        buf.write("\7\60\2\2\u018c=\3\2\2\2\u018d\u018e\7.\2\2\u018e?\3\2")
        buf.write("\2\2\u018f\u0190\7,\2\2\u0190A\3\2\2\2\u0191\u0192\7\61")
        buf.write("\2\2\u0192C\3\2\2\2\u0193\u0194\7-\2\2\u0194E\3\2\2\2")
        buf.write("\u0195\u0196\7/\2\2\u0196G\3\2\2\2\u0197\u0198\7~\2\2")
        buf.write("\u0198\u0199\7~\2\2\u0199I\3\2\2\2\u019a\u019b\7*\2\2")
        buf.write("\u019bK\3\2\2\2\u019c\u019d\7+\2\2\u019dM\3\2\2\2\u019e")
        buf.write("\u019f\7a\2\2\u019f\u01a0\7}\2\2\u01a0O\3\2\2\2\u01a1")
        buf.write("\u01a2\7\177\2\2\u01a2Q\3\2\2\2\u01a3\u01a4\7<\2\2\u01a4")
        buf.write("\u01a5\7/\2\2\u01a5S\3\2\2\2\u01a6\u01a7\7<\2\2\u01a7")
        buf.write("U\3\2\2\2\u01a8\u01a9\7>\2\2\u01a9\u01aa\7?\2\2\u01aa")
        buf.write("W\3\2\2\2\u01ab\u01ac\7>\2\2\u01ac\u01ad\7@\2\2\u01ad")
        buf.write("Y\3\2\2\2\u01ae\u01af\7@\2\2\u01af\u01b0\7?\2\2\u01b0")
        buf.write("[\3\2\2\2\u01b1\u01b2\7>\2\2\u01b2]\3\2\2\2\u01b3\u01b4")
        buf.write("\7?\2\2\u01b4_\3\2\2\2\u01b5\u01b6\7@\2\2\u01b6a\3\2\2")
        buf.write("\2\u01b7\u01b8\7^\2\2\u01b8\u01b9\7n\2\2\u01b9\u01ba\7")
        buf.write("k\2\2\u01ba\u01bb\7u\2\2\u01bb\u01bc\7v\2\2\u01bcc\3\2")
        buf.write("\2\2\u01bd\u01be\7^\2\2\u01be\u01bf\7e\2\2\u01bf\u01c0")
        buf.write("\7n\2\2\u01c0\u01c1\7g\2\2\u01c1\u01c2\7c\2\2\u01c2\u01c3")
        buf.write("\7t\2\2\u01c3e\3\2\2\2\u01c4\u01c5\7^\2\2\u01c5\u01c6")
        buf.write("\7u\2\2\u01c6\u01c7\7c\2\2\u01c7\u01c8\7x\2\2\u01c8\u01c9")
        buf.write("\7g\2\2\u01c9g\3\2\2\2\u01ca\u01cb\7^\2\2\u01cb\u01cc")
        buf.write("\7u\2\2\u01cc\u01cd\7q\2\2\u01cd\u01ce\7w\2\2\u01ce\u01cf")
        buf.write("\7t\2\2\u01cf\u01d0\7e\2\2\u01d0\u01d1\7g\2\2\u01d1i\3")
        buf.write("\2\2\2\u01d2\u01d3\7^\2\2\u01d3\u01d4\7s\2\2\u01d4\u01d5")
        buf.write("\7w\2\2\u01d5\u01d6\7k\2\2\u01d6\u01d7\7v\2\2\u01d7k\3")
        buf.write("\2\2\2\u01d8\u01d9\7^\2\2\u01d9\u01da\7u\2\2\u01da\u01db")
        buf.write("\7s\2\2\u01db\u01dc\7n\2\2\u01dc\u01dd\7g\2\2\u01dd\u01de")
        buf.write("\7z\2\2\u01de\u01df\7g\2\2\u01df\u01e0\7e\2\2\u01e0\u01e1")
        buf.write("\3\2\2\2\u01e1\u01e2\b\66\3\2\u01e2m\3\2\2\2\u01e3\u01e4")
        buf.write("\t\6\2\2\u01e4o\3\2\2\2\u01e5\u01e6\t\7\2\2\u01e6q\3\2")
        buf.write("\2\2\u01e7\u01e8\t\b\2\2\u01e8s\3\2\2\2\u01e9\u01ea\t")
        buf.write("\t\2\2\u01eau\3\2\2\2\u01eb\u01ec\t\n\2\2\u01ecw\3\2\2")
        buf.write("\2\u01ed\u01ee\t\13\2\2\u01eey\3\2\2\2\u01ef\u01f0\t\f")
        buf.write("\2\2\u01f0{\3\2\2\2\u01f1\u01f2\t\r\2\2\u01f2}\3\2\2\2")
        buf.write("\u01f3\u01f4\t\16\2\2\u01f4\177\3\2\2\2\u01f5\u01f6\t")
        buf.write("\17\2\2\u01f6\u0081\3\2\2\2\u01f7\u01f8\t\20\2\2\u01f8")
        buf.write("\u0083\3\2\2\2\u01f9\u01fa\t\21\2\2\u01fa\u0085\3\2\2")
        buf.write("\2\u01fb\u01fc\t\22\2\2\u01fc\u0087\3\2\2\2\u01fd\u01fe")
        buf.write("\t\23\2\2\u01fe\u0089\3\2\2\2\u01ff\u0200\t\24\2\2\u0200")
        buf.write("\u008b\3\2\2\2\u0201\u0202\t\25\2\2\u0202\u008d\3\2\2")
        buf.write("\2\u0203\u0204\t\26\2\2\u0204\u008f\3\2\2\2\u0205\u0206")
        buf.write("\t\27\2\2\u0206\u0091\3\2\2\2\u0207\u0208\t\30\2\2\u0208")
        buf.write("\u0093\3\2\2\2\u0209\u020a\t\31\2\2\u020a\u0095\3\2\2")
        buf.write("\2\u020b\u020c\t\32\2\2\u020c\u0097\3\2\2\2\u020d\u020e")
        buf.write("\t\33\2\2\u020e\u0099\3\2\2\2\u020f\u0210\t\34\2\2\u0210")
        buf.write("\u009b\3\2\2\2\u0211\u0212\t\35\2\2\u0212\u009d\3\2\2")
        buf.write("\2\u0213\u0214\t\36\2\2\u0214\u009f\3\2\2\2\u0215\u0216")
        buf.write("\t\37\2\2\u0216\u00a1\3\2\2\2\u0217\u0219\4\62;\2\u0218")
        buf.write("\u0217\3\2\2\2\u0219\u021a\3\2\2\2\u021a\u0218\3\2\2\2")
        buf.write("\u021a\u021b\3\2\2\2\u021b\u00a3\3\2\2\2\u021c\u021e\5")
        buf.write("\4\2\2\u021d\u021c\3\2\2\2\u021d\u021e\3\2\2\2\u021e\u021f")
        buf.write("\3\2\2\2\u021f\u023b\5N\'\2\u0220\u0228\7)\2\2\u0221\u0227")
        buf.write("\n\3\2\2\u0222\u0224\7\17\2\2\u0223\u0222\3\2\2\2\u0223")
        buf.write("\u0224\3\2\2\2\u0224\u0225\3\2\2\2\u0225\u0227\7\f\2\2")
        buf.write("\u0226\u0221\3\2\2\2\u0226\u0223\3\2\2\2\u0227\u022a\3")
        buf.write("\2\2\2\u0228\u0226\3\2\2\2\u0228\u0229\3\2\2\2\u0229\u022b")
        buf.write("\3\2\2\2\u022a\u0228\3\2\2\2\u022b\u023a\7)\2\2\u022c")
        buf.write("\u0234\7$\2\2\u022d\u0233\n \2\2\u022e\u0230\7\17\2\2")
        buf.write("\u022f\u022e\3\2\2\2\u022f\u0230\3\2\2\2\u0230\u0231\3")
        buf.write("\2\2\2\u0231\u0233\7\f\2\2\u0232\u022d\3\2\2\2\u0232\u022f")
        buf.write("\3\2\2\2\u0233\u0236\3\2\2\2\u0234\u0232\3\2\2\2\u0234")
        buf.write("\u0235\3\2\2\2\u0235\u0237\3\2\2\2\u0236\u0234\3\2\2\2")
        buf.write("\u0237\u023a\7$\2\2\u0238\u023a\n!\2\2\u0239\u0220\3\2")
        buf.write("\2\2\u0239\u022c\3\2\2\2\u0239\u0238\3\2\2\2\u023a\u023d")
        buf.write("\3\2\2\2\u023b\u023c\3\2\2\2\u023b\u0239\3\2\2\2\u023c")
        buf.write("\u023e\3\2\2\2\u023d\u023b\3\2\2\2\u023e\u023f\5P(\2\u023f")
        buf.write("\u0240\3\2\2\2\u0240\u0241\bR\4\2\u0241\u00a5\3\2\2\2")
        buf.write("\31\2\3\u00a9\u00b2\u00b4\u00c2\u00f4\u00f7\u00f9\u0101")
        buf.write("\u0105\u010a\u0110\u021a\u021d\u0223\u0226\u0228\u022f")
        buf.write("\u0232\u0234\u0239\u023b\5\b\2\2\7\3\2\6\2\2")
        return buf.getvalue()


class RALexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    SQLEXEC_MODE = 1

    WS = 1
    COMMENT = 2
    LINE_COMMENT = 3
    TERMINATOR = 4
    FORCE = 5
    IS_NULL = 6
    IS_NOT_NULL = 7
    IS = 8
    NULL = 9
    LIKE = 10
    AND = 11
    OR = 12
    NOT = 13
    STRING = 14
    NUMBER = 15
    ID = 16
    RENAME = 17
    PROJECT = 18
    SELECT = 19
    JOIN = 20
    LEFT_OUTER_JOIN = 21
    RIGHT_OUTER_JOIN = 22
    FULL_OUTER_JOIN = 23
    CROSS = 24
    UNION = 25
    DIFF = 26
    INTERSECT = 27
    AGGR = 28
    DOT = 29
    COMMA = 30
    STAR = 31
    SLASH = 32
    PLUS = 33
    MINUS = 34
    CONCAT = 35
    PAREN_L = 36
    PAREN_R = 37
    ARG_L = 38
    ARG_R = 39
    GETS = 40
    COLON = 41
    LE = 42
    NE = 43
    GE = 44
    LT = 45
    EQ = 46
    GT = 47
    LIST = 48
    CLEAR = 49
    SAVE = 50
    SOURCE = 51
    QUIT = 52
    SQLEXEC = 53
    SQLEXEC_TEXT = 54

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE", "SQLEXEC_MODE" ]

    literalNames = [ "<INVALID>",
            "';'", "'!'", "'\\rename'", "'\\project'", "'\\select'", "'\\join'", 
            "'\\left_outer_join'", "'\\right_outer_join'", "'\\full_outer_join'", 
            "'\\cross'", "'\\union'", "'\\diff'", "'\\intersect'", "'\\aggr'", 
            "'.'", "','", "'*'", "'/'", "'+'", "'-'", "'||'", "'('", "')'", 
            "'_{'", "'}'", "':-'", "':'", "'<='", "'<>'", "'>='", "'<'", 
            "'='", "'>'", "'\\list'", "'\\clear'", "'\\save'", "'\\source'", 
            "'\\quit'", "'\\sqlexec'" ]

    symbolicNames = [ "<INVALID>",
            "WS", "COMMENT", "LINE_COMMENT", "TERMINATOR", "FORCE", "IS_NULL", 
            "IS_NOT_NULL", "IS", "NULL", "LIKE", "AND", "OR", "NOT", "STRING", 
            "NUMBER", "ID", "RENAME", "PROJECT", "SELECT", "JOIN", "LEFT_OUTER_JOIN", 
            "RIGHT_OUTER_JOIN", "FULL_OUTER_JOIN", "CROSS", "UNION", "DIFF", 
            "INTERSECT", "AGGR", "DOT", "COMMA", "STAR", "SLASH", "PLUS", 
            "MINUS", "CONCAT", "PAREN_L", "PAREN_R", "ARG_L", "ARG_R", "GETS", 
            "COLON", "LE", "NE", "GE", "LT", "EQ", "GT", "LIST", "CLEAR", 
            "SAVE", "SOURCE", "QUIT", "SQLEXEC", "SQLEXEC_TEXT" ]

    ruleNames = [ "WS", "COMMENT", "LINE_COMMENT", "TERMINATOR", "FORCE", 
                  "IS_NULL", "IS_NOT_NULL", "IS", "NULL", "LIKE", "AND", 
                  "OR", "NOT", "STRING", "NUMBER", "ID", "RENAME", "PROJECT", 
                  "SELECT", "JOIN", "LEFT_OUTER_JOIN", "RIGHT_OUTER_JOIN", 
                  "FULL_OUTER_JOIN", "CROSS", "UNION", "DIFF", "INTERSECT", 
                  "AGGR", "DOT", "COMMA", "STAR", "SLASH", "PLUS", "MINUS", 
                  "CONCAT", "PAREN_L", "PAREN_R", "ARG_L", "ARG_R", "GETS", 
                  "COLON", "LE", "NE", "GE", "LT", "EQ", "GT", "LIST", "CLEAR", 
                  "SAVE", "SOURCE", "QUIT", "SQLEXEC", "A", "B", "C", "D", 
                  "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", 
                  "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", 
                  "UNSIGNED_INTEGER_FRAGMENT", "SQLEXEC_TEXT" ]

    grammarFileName = "RALexer.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


